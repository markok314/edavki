<?xml version="1.0" encoding="utf-8"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:doc="http://edavki.durs.si/Documents/Schemas/Doh_KDVP_9.xsd" xmlns:form="http://tempuri.org/Forms" xmlns:incl="urn:hermes-softlab-com-durs-include" xmlns:edp="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd" xmlns:loc="urn:hermes-softlab-com-durs-localize"><xsl:template match="/"><html xmlns:var="urn:hermes-softlab-com-durs-variant"><head><style type="text/css" xmlns:outxsl="generated:xsl" xmlns:edp="http://tempuri.org/EDP-Common-1" xmlns:loc="http://tempuri.org/EDP-l10n">

/*** EDP-Style 2.0 ***/

.lblprefill { border: 1px solid #B4BCB9; background-color: #F0F0F0; color:#000}
div.fldprefill, div.fldborder, span.fldborder {  border: 1px solid #cfa1a0; padding: 2px; }
div.fldinput, div.fldValue, span.fldinput { background: white; border: 1px solid #cfa1a0; padding: 2px;  }

   /*** font-size general ***/
body, table { font-size: 9pt;}
body.sign { border: 0; background: #efefef; font-family: verdana, sans-serif;}
table {border: 0px; }

.mw50p {min-width:50%;}

   /*** COMMON HEADER ***/
div.commonheader {display: block;  padding: 0.6em 1em; /*border-color: #00659c;*/ 
background-image: url('durs_znak_60.png'); background-attachment:scroll;  background-position:bottom right; background-repeat:no-repeat;  background-attachment:scroll;}
div.commonheader table td {color: #000;  }

   /*** Signing ***/
div.signareafile { display: block; height: expression('100%'); background: #f2f2f2; padding: 5px 15px; margin-bottom: 20px; }
div.signareafile h2, div.signareaprep h2 {margin-top: 0; margin-bottom: 0.3em; }

   /*** Headings ***/
.signareafile h1 {font-size: 26px; }
.signareafile h2 {font-size: 20px; }
.signareafile h3 {font-size: 18px; }
.signareafile h4 {font-size: 16px; }
.signareafile h5 {font-size: 14px; }
.signareafile h6 {font-size: 12px; }

.signareafile p  {margin: 0 0 0.1em 0px; line-height: 1.3em;}
.signareafile li {margin-bottom: 3px;}
.signareafile ul {margin:0; padding: 4pt 2.4em;}
.signareafile li ul {margin:0; padding: 2pt 1.5em;}

.signareafile .instruct {margin-bottom: 0.5em; color: #343434; }
.signareafile .instruct1 {margin-bottom: 0em; color: #343434; }
.signareafile .instruc2 {display: none;}
.signareafile .hidden {display: none;}
.signareafile .no-margin {margin: 0;}
.signareafile .right { text-align: right;}

.bgTitle1 {background-color: #C8D3D7; color: #1c1c1c;padding: 2px 5px 1px 5px;}
.bgTitle2 {background-color: #D6DEE1; color: #1c1c1c;padding: 2px 5px 1px 5px; font-size: small;}
.bgTitle3 {background-color: #E4E9EC; color: #1c1c1c;padding: 2px 5px 1px 5px; font-size: small;}
.padd0 {padding: 0 !important;}
.cTitle1 {color: #C8D3D7;}
.cTitle2 {color: #D2DADD;}
.cTitle3 {color: #E4E9EC;}

.bold { font-weight: bold; }

   /*** TABLE SIGN BULLET ***/
div.tableframe-nc {display: inline-block;}   
.currencyright {font-size: 11px !important;text-align: right;}
div.tablebackground {background-color: #e2e2e2;}
table.tablesignbullet {margin: 0.2em 0px 10px 0px; border: #738294 1px solid; background-color: #fafafa;  }
table.tablesignbullet tr.noborder td {border: 0px none; background-color: #f8f8f8; }
table.tablesignbullet td {padding: 0.3em 6px 0.3em 4px; border-bottom: #f1f1f1 1px solid; vertical-align: middle;}
table.tablesignbullet tr.first td { background: #b8ccce; border-bottom: #016a73 2px solid; font-weight: bold;}
table.tablesignbullet tr.first1 td { background: #D3DDDE ; border-bottom: #308289 1px solid}
table.tablesignbullet tr.First td { background: #b8ccce; border-bottom: #016a73 2px solid; font-weight: bold;}
table.tablesignbullet td.value {text-align: center; }
table.tablesignbullet td.right {text-align: right; }  
table.tablesignbullet td.input {padding-top: 0; padding-bottom: 0; }
table.tablesignbullet td.dark {background: #efefef; }
table.tablesignbullet td.lblmenu {background: #f1f1f1; border-bottom: #e8e8e8 0px solid;}
table.tablesignbullet tr.subhead {font-weight: bold; background-color: #e9e9e9;}
table.tablesignbullet tr.lblheader  {background-color: #f2f2f2;}
table.tablesignbullet tr.lblheader0  {font-weight: bold;  background-color: #dddddd;}
table.tablesignbullet tr.lblheader2 {font-weight: bold; background-color: #e9e9e9; }
table.tablesignbullet tr.lblheader3 {font-weight: bold; background-color: #f3f3f3; }
table.tablesignbullet tr.empty td {background:#f2f2f2; border-bottom: #738294 1px solid; border-top:none; line-height:3px;}
table.tablesignbullet tr.summary td {background:#f5f5f5; font-weight:bold; border-top: #738294 2px solid ;}
table.tablesignbullet tr.lSummary td, tfoot.lSummary td {background: #D9D9D9; font-weight:bold; border-top: #C4C4C4 1px solid;}
table.tablesignbullet tr.grey td {background:#c8d3d4; line-height:0.6em;}
td.tdcomment {background-color: #f3f3f3; color: #343434;}
.hyphenate {overflow-wrap: break-word;word-wrap: break-word;-webkit-hyphens: auto;-ms-hyphens: auto;-moz-hyphens: auto;hyphens: auto;}

   /*** TABLE SIGN SIMPLE ***/
table.tablesignsimple {margin: 0.2em 0px 10px 25px; }
table.tablesignsimple td {padding: 3px 8px 3px 0px; vertical-align: middle; }

   /*** TABLE SIGN FORM ***/
table.tablesignform {border-top: 1px solid #b9d3e1; width: 100%; margin-top: 15px; }
table.tablesignform td.right {text-align: right; }   

   /*** TABLE FORMDDVO ***/
table.formddvo td {vertical-align: top; padding: 4px 4px 3px 4px; font: normal 8pt verdana, sans-serif;  }

   /*** TABLE FORMINPUT ***/
table.forminput {width: 100%;}
table.forminput td {}
table.forminput td.no {text-align: right; width: 20px;}
table.forminput td.input {padding: 0px 2px; width: 105px;}

   /*** TABLE FORMDOH ***/

table.doh tr.subhead td  {text-align: center; vertical-align: middle; padding-left: 5px; padding-right: 5px;}
table.doh tr.subhead {font-weight: normal; }
table.doh td.input {vertical-align: top; padding-left: 5px; padding-right: 5px; text-align: center;}
table.doh td.lblno {vertical-align: top; padding-left: 5px; padding-right: 5px; padding-bottom: 3px; padding-top: 6px; }
table.doh td.lbl  { padding-right: 1px; padding-left: 1px; padding-bottom: 3px; vertical-align: top; padding-top: 6px; }
table.doh td.dark {background: #ffffff; }
table.doh tr.subhead td.dark {background-color: #e0e0e0; }

TABLE.doh A { padding-right: 7px; display: block; padding-left: 7px; padding-bottom: 0.35em; padding-top: 0.35em; }
TABLE.doh A:hover { background: #00659c; color: #fff; }
TABLE.doh DIV P { margin: 7px; color: #00659c; }
TABLE.doh DIV LI { margin: 7px; color: #00659c; line-height: 1.2em; }
TABLE.doh SELECT, table.doh table.insert { margin-top: 1px; }

table.doh table.insert td {padding: 2px; }
table.doh table.insert td.input {padding-top: 0px; }
table.doh table.insert td.lbl {padding-top: 5px; }

/*** TABLE BULLETFULL margin: 0.2em 0px 10px 5px;  ***/

table.tablebulletFull td.tblhdr {margin:0; padding:0; border:0;}
table.tablesignbullet td.tblhdr {margin:0; padding:0; border:0;}
table.tblHeader {margin:0; padding:0; border:none 0; width:100% ;  }
table.tblHeader td.tdHeader1  { text-align:center; FONT-WEIGHT: bold; width:5em; BACKGROUND: #016a73;  COLOR: #fff; BORDER-BOTTOM: #016a73 2px solid}
table.tblHeader td.tdHeader2 {FONT-WEIGHT: bold; padding-left:1em;  BACKGROUND: #b8ccce; BORDER-BOTTOM: #016a73 2px solid}

td.mark1st {border-top:10px #D2DADD solid}
div.indent3 {padding-left: 3em; text-indent:-3em;}

table.tablebulletFull {margin: 0.2em 0px 10px 0px; border: 1px solid #738294; background: #ffffff;  width: 100%; }
table.tablebulletFull td {padding: 0.3em 8px 0.3em 8px; border-bottom: 1px solid #efefef; vertical-align: top;}
table.tablebulletFull tr.first td {background: #b8ccce; border-bottom: #016a73 2px solid; font-weight: bold;}
table.tablebulletFull tr.first1 td { background: #D3DDDE ; border-bottom: #308289 1px solid}
table.tablebulletFull td.lblFirst {background-color: #016A73; color:#fff ; width:5em;}
table.tablebulletFull tr.firstX td {border-left: #016A73 5em solid; border-bottom: #016A73 2px solid; background: #B8CCCE; font-weight:bold;}
table.tablebulletFull span.firstX { left: -4em; position: relative; color:#fff;}
table.tablebulletFull span.firstXx { left: -2em; position: relative;}
table.tablebulletFull tr.lblheader {font-weight: bold; background-color: #d9d9d9; }
table.tablebulletFull tr.lblheader2 {font-weight: bold; background-color: #e4e4e4; }
table.tablebulletFull tr.lblheader3 {font-weight: bold; background-color: #eeeeee; }
table.tablebulletFull tr.subhead td img {margin: 0px 2px;}
table.tablebulletFull tr.oddRow {background: #f3f3f3; }
table.tablebulletFull tr.evenRow {background: #ffffff; }
table.tablebulletFull td.lbl {padding-top: 0.45em; }
table.tablebulletFull td.lblmenu {background: #f1f1f1; border-bottom: #eaeaea 0px solid;}
table.tablebulletFull td.lblno {padding-top: 0.45em;}
table.tablebulletFull td.valuebld {padding-top: 0.45em; font-weight: bold;}
table.tablebulletFull td.input {padding-top: 0.45em; }
table.tablebulletFull td.dark {background: #eeeeee; }
table.tablebulletFull td.subtable { padding: 0; margin:0}
table.tablebulletFull DIV P { MARGIN: 7px; COLOR: #343434; }
table.tablebulletFull tr.lHeader td {background: #f1f1f1;}
table.tablebulletFull tr.lSummary td {background: #D9D9D9; font-weight:bold; border-top: #C4C4C4 1px solid;}
table.tablebulletFull tr.empty td {background:#f3f3f3; border-bottom: #c1c7cf 1px solid; border-top:none; line-height:3px;}

table.tableSubTable {margin: 0.2em 0px 1px 0px; width:100%; }
table.tableSubTable tr.oddRow {background: #f1f1f1; }
table.tableSubTable tr.evenRow {background: #ffffff; }

table.evenoddrow1&gt;tbody&gt;tr:nth-child(even) {background: #fefefe}
table.evenoddrow1&gt;tbody&gt;tr:nth-child(odd) {background: #f6f6f6} 


table.evenoddrow tr:nth-child(even) {background: #f8f8f8}
table.evenoddrow tr:nth-child(odd) {background: #e4e4e4;} 

table.evenoddrow tr:nth-child(even) td table.inevenodd tr {background: #f8f8f8;} 
table.evenoddrow tr:nth-child(odd) td table.inevenodd tr {background: #e4e4e4;} 
table.evenoddrow tr:nth-child(even) td table.inevenodd tr td.inp {background: #fefefe;}
table.evenoddrow tr:nth-child(odd) td table.inevenodd tr td.inp {background: #f6f6f6;} 

table.evenoddrow td table.inevenodd tr td {border-bottom: 0px solid;} 
table.evenoddrow tr:nth-child(even)  table.tablebullet tr {background: initial;} 
table.evenoddrow tr:nth-child(odd) table.tablebullet tr {background: initial;} 

table.full { width: 100%;}

.bgpadd1 {border-left-color: #f1f1f1; border-left-style: solid;border-left-width: 2em;}
.bgpadd2 {border-left-color: #f1f1f1; border-left-style: solid;border-left-width: 4em;}
.bgpadd3 {border-left-color: #f1f1f1; border-left-style: solid;border-left-width: 6em;}



    </style></head><body class="sign"><xsl:call-template name="Doh_KDVP"></xsl:call-template></body></html></xsl:template><xsl:template name="Doh_KDVP"><xsl:for-each select="doc:Envelope"><xsl:if test="//edp:Signatures/edp:DepositorSignature" xmlns:var="urn:hermes-softlab-com-durs-variant" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><xsl:if test="string(//edp:Signatures/edp:DepositorSignature/ds:Signature/ds:SignatureValue)=''"><h1>Oddajanje vloge</h1><p class="instruct">S potrditvijo boste podpisali dokument in ga predložili finančni upravi. Vloženega dokumenta ne morete več spreminjati.</p></xsl:if></xsl:if><xsl:if test="//edp:Signatures/edp:PreparerSignature" xmlns:var="urn:hermes-softlab-com-durs-variant" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><xsl:if test="string(//edp:Signatures/edp:PreparerSignature/ds:Signature/ds:SignatureValue)=''"><h1>Priprava vloge</h1><p class="instruct">S potrditvijo boste pripravili dokument za podpisnika. Dokument ne bo predložen finančni upravi.</p></xsl:if></xsl:if><xsl:if test="//edp:Signatures/edp:DepositorServerSignature" xmlns:var="urn:hermes-softlab-com-durs-variant" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><xsl:if test="string(//edp:Signatures/edp:DepositorServerSignature/ds:Signature/ds:SignatureValue)=''"><h1>Oddajanje vloge</h1><p class="instruct">S potrditvijo boste podpisali dokument in ga predložili finančni upravi. Vloženega dokumenta ne morete več spreminjati.</p></xsl:if></xsl:if><xsl:if test="//edp:Signatures/edp:PreparerServerSignature" xmlns:var="urn:hermes-softlab-com-durs-variant" xmlns:ds="http://www.w3.org/2000/09/xmldsig#"><xsl:if test="string(//edp:Signatures/edp:PreparerServerSignature/ds:Signature/ds:SignatureValue)=''"><h1>Priprava vloge</h1><p class="instruct">S potrditvijo boste pripravili dokument za podpisnika. Dokument ne bo predložen finančni upravi.</p></xsl:if></xsl:if><div class="document"><div class="commonheader"><table cellspacing="0"><tbody><xsl:for-each select="edp:Header//edp:domain"><tr><td><b>Sistem:</b></td><td><xsl:value-of select="."></xsl:value-of></td></tr></xsl:for-each><xsl:for-each select="edp:Header//edp:responseTo"><tr><td><b>Odgovor na dokument:</b></td><td><xsl:value-of select="."></xsl:value-of></td></tr></xsl:for-each><xsl:for-each select="edp:Signatures//edp:receipt"><tr><td><b>Vloženo:</b></td><td><xsl:call-template name="dateAndTime"><xsl:with-param name="timestamp"><xsl:value-of select="edp:timestamp"></xsl:value-of></xsl:with-param></xsl:call-template></td></tr><xsl:if test="string-length(edp:documentNumber)!=0"><tr><td><b>Št. dokumenta:</b></td><td><xsl:value-of select="edp:documentNumber"></xsl:value-of></td></tr></xsl:if><xsl:if test="string-length(edp:metaText1)!=0 and string-length(edp:metaDate1)!=0 and starts-with(edp:metaText1, 'SI')"><tr><td><b>Referenčna številka:</b></td><td><xsl:value-of select="edp:metaText1"></xsl:value-of></td></tr><tr><td><b>Verzija:
                  </b></td><td><xsl:call-template name="dateAndTime"><xsl:with-param name="timestamp"><xsl:value-of select="edp:metaDate1"></xsl:value-of></xsl:with-param></xsl:call-template></td></tr></xsl:if></xsl:for-each><xsl:for-each select="edp:Signatures/edp:DepositorSignature/edp:Depositor"><tr><td><b>Vložil:</b></td><td><xsl:value-of select="edp:name"></xsl:value-of></td><td>                
                (<xsl:call-template name="dateAndTime"><xsl:with-param name="timestamp"><xsl:value-of select="edp:timestamp"></xsl:value-of></xsl:with-param></xsl:call-template>)
              </td></tr></xsl:for-each><xsl:for-each select="edp:Signatures/edp:DepositorServerSignature/edp:Depositor"><tr><td><b>Vložil:</b></td><td><xsl:value-of select="edp:name"></xsl:value-of></td></tr></xsl:for-each><xsl:for-each select="edp:Signatures/edp:PreparerSignature/edp:Preparer"><tr><td><b>Pripravil:</b></td><td><xsl:value-of select="edp:name"></xsl:value-of></td><td>
                (<xsl:call-template name="dateAndTime"><xsl:with-param name="timestamp"><xsl:value-of select="edp:timestamp"></xsl:value-of></xsl:with-param></xsl:call-template>)
              </td></tr></xsl:for-each><xsl:for-each select="edp:Signatures/edp:PreparerServerSignature/edp:Preparer"><tr><td><b>Pripravil:</b></td><td><xsl:value-of select="edp:name"></xsl:value-of></td></tr></xsl:for-each></tbody></table><p> </p></div><div class="signareafile"><xsl:for-each select="doc:body/doc:Doh_KDVP"><h1>Napoved za odmero dohodnine od dobička od odsvojitve vrednostnih papirjev in drugih deležev ter investicijskih kuponov</h1><p>Za obdobje od  
                <xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:KDVP/doc:PeriodStart"></xsl:with-param></xsl:call-template> 
              do 
             <xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:KDVP/doc:PeriodEnd"></xsl:with-param></xsl:call-template></p><p> </p><p>Vrsta dokumenta 
              <xsl:if test="doc:KDVP/doc:DocumentWorkflowID"><xsl:value-of select="doc:KDVP/doc:DocumentWorkflowID"></xsl:value-of></xsl:if></p><p> </p><table class="tablesignbullet" id="tableTaxPayerInfo" cellspacing="0"><tr class="first"><td colspan="5">Podatki o zavezancu</td></tr><tr><td>Ime in priimek:</td><td><xsl:value-of select="//edp:Header/edp:taxpayer/edp:name"></xsl:value-of> </td></tr><tr><td>Davčna številka:</td><td><xsl:value-of select="//edp:Header/edp:taxpayer/edp:taxNumber"></xsl:value-of> </td></tr><tr><td>Naslov:</td><td><div><xsl:value-of select="//edp:Header/edp:taxpayer/edp:address1"></xsl:value-of></div><xsl:if test="not(string(//edp:Header/edp:taxpayer/edp:address2)='')"><div><xsl:value-of select="//edp:Header/edp:taxpayer/edp:address2"></xsl:value-of></div> 
                  </xsl:if><div><xsl:value-of select="//edp:Header/edp:taxpayer/edp:postNumber"></xsl:value-of> <xsl:value-of select="//edp:Header/edp:taxpayer/edp:city"></xsl:value-of></div></td></tr><tr><td>Država rezidentstva:</td><td><xsl:choose><xsl:when test="//doc:body/doc:Doh_KDVP/doc:KDVP/doc:IsResident='true'">Zavezanec je rezident Republike Slovenije.</xsl:when><xsl:otherwise><xsl:value-of select="//doc:body/doc:Doh_KDVP/doc:KDVP/doc:CountryOfResidenceName"></xsl:value-of> 
                  </xsl:otherwise></xsl:choose></td></tr><tr><td>Elektronski naslov:</td><td><xsl:value-of select="//doc:body/doc:Doh_KDVP/doc:KDVP/doc:Email"></xsl:value-of> 
                </td></tr><tr><td>Telefonska številka:</td><td><xsl:value-of select="//doc:body/doc:Doh_KDVP/doc:KDVP/doc:TelephoneNumber"></xsl:value-of> 
                </td></tr></table><xsl:for-each select="doc:KDVP"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td colspan="5">Podatki za odmero dohodnine od dobička od odsvojitve vrednostnih papirjev in drugih deležev ter investicijskih kuponov </td></tr><tr class="lblheader2"><td>Vrsta popisnega lista</td><td>Število</td></tr><tr align="right"><td align="left">Popisni list vrednostnega papirja oziroma investicijskega kupona </td><td align="right"><xsl:value-of select="doc:SecurityCount"></xsl:value-of> </td></tr><tr align="right"><td align="left">Popisni list vrednostnega papirja oziroma investicijskega kupona za posle na kratko </td><td align="right"><xsl:value-of select="doc:SecurityShortCount"></xsl:value-of> </td></tr><tr align="right"><td align="left">Popisni list vrednostnega papirja oziroma investicijskega kupona, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju </td><td align="right"><xsl:value-of select="doc:SecurityWithContractCount"></xsl:value-of> </td></tr><tr align="right"><td align="left">Popisni list vrednostnega papirja oziroma investicijskega kupona za posle na kratko, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju </td><td align="right"><xsl:value-of select="doc:SecurityWithContractShortCount"></xsl:value-of> </td></tr><tr align="right"><td align="left">Popisni list deleža v gospodarskih družbah, zadrugah in drugih oblikah organiziranja </td><td align="right"><xsl:value-of select="doc:ShareCount"></xsl:value-of> </td></tr></table><xsl:choose><xsl:when test="//doc:body/doc:Doh_KDVP/doc:KDVPItem/doc:TaxDecreaseConformance = 'true'"><p> </p><h3>Oprostitev plačila dohodnine od dobička iz kapitala, doseženega pri odsvojitvi deleža, pridobljenega na podlagi naložb tveganega kapitala po določbi 5. točke drugega odstavka 96. člena ZDOH-2</h3><p> </p><p>Uveljavljam oprostitev iz 5. točke drugega odstavka 96. člena ZDoh-2: Da</p><p> </p><p>Podpisani izjavljam:</p><p>da sem odsvojil delež, pridobljen na podlagi naložb tveganega kapitala (naložb v obliki povečanja osnovnega kapitala družbe z vložki zavezanca ali ustanovitve gospodarske družbe) v družbi tveganega kapitala, ki je ustanovljena v skladu z zakonom, ki ureja družbe tveganega kapitala in je imela ta družba status družbe tveganega kapitala skozi celotno obdobje imetništva takega deleža zavezanca. </p><p> </p></xsl:when><xsl:otherwise><p> </p><h3>Oprostitev plačila dohodnine od dobička iz kapitala, doseženega pri odsvojitvi deleža, pridobljenega na podlagi naložb tveganega kapitala po določbi 5. točke drugega odstavka 96. člena ZDOH-2</h3><p> </p><p>Uveljavljam oprostitev iz 5. točke drugega odstavka 96. člena ZDoh-2: Ne</p><p> </p><p>Podpisani izjavljam:</p><p>da sem odsvojil delež, pridobljen na podlagi naložb tveganega kapitala (naložb v obliki povečanja osnovnega kapitala družbe z vložki zavezanca ali ustanovitve gospodarske družbe) v družbi tveganega kapitala, ki je ustanovljena v skladu z zakonom, ki ureja družbe tveganega kapitala in je imela ta družba status družbe tveganega kapitala skozi celotno obdobje imetništva takega deleža zavezanca. </p><p> </p></xsl:otherwise></xsl:choose><xsl:if test="//doc:body/doc:Doh_KDVP/doc:TaxBaseDecrease"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td colspan="5">Znižanje pozitivne davčne osnove za preneseno izgubo (tretji odstavek 97. člena ZDoh-2)</td></tr><tr class="lblheader2"><td>Zap. št.</td><td>Številka odločbe</td><td>Datum odločbe</td><td>Finančni urad</td><td>Izguba (v EUR s centi)</td></tr><xsl:for-each select="//doc:body/doc:Doh_KDVP/doc:TaxBaseDecrease"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td align="right"><xsl:value-of select="doc:OrderNumber"></xsl:value-of> </td><td align="right"><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:OrderDate"></xsl:with-param></xsl:call-template> </td><td align="right"><xsl:value-of select="doc:TaxOfficeName"></xsl:value-of> </td><td align="right"><xsl:call-template name="Amount2"><xsl:with-param name="amount" select="doc:Loss"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table></xsl:if><xsl:if test="//doc:body/doc:Doh_KDVP/doc:TaxRelief"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td colspan="6">Znižanje pozitivne davčne osnove oziroma dobička za izgubo, doseženo pri odsvojitvi nepremičnin</td></tr><tr class="lblheader2"><td>Zap. št.</td><td>Datum napovedi</td><td>Številka odločbe</td><td>Izguba (v EUR s centi)</td><td>Dobiček (v EUR s centi)</td><td>Odmerjena dohodnina (v EUR s centi)</td></tr><xsl:for-each select="//doc:body/doc:Doh_KDVP/doc:TaxRelief"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td align="right"><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:OrderDate"></xsl:with-param></xsl:call-template> </td><td align="right"><xsl:value-of select="doc:OrderNumber"></xsl:value-of> </td><td align="right"><xsl:call-template name="Amount2"><xsl:with-param name="amount" select="doc:Loss"></xsl:with-param></xsl:call-template> </td><td align="right"><xsl:call-template name="Amount2"><xsl:with-param name="amount" select="doc:Profit"></xsl:with-param></xsl:call-template> </td><td align="right"><xsl:call-template name="Amount2"><xsl:with-param name="amount" select="doc:IncomeTax"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table></xsl:if><xsl:if test="//doc:body/doc:Doh_KDVP/doc:KDVPItem"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td colspan="6">Uveljavljanje odbitka davka, plačanega v tujini, oziroma oprostitve</td></tr><tr class="lblheader2"><td>Zap. št.</td><td>Oznaka kapitala, od katerega je bil ustvarjen dobiček v tujini*</td><td>Tuji davek (v EUR s centi)</td><td>Država</td></tr><xsl:for-each select="//doc:body/doc:Doh_KDVP/doc:KDVPItem"><xsl:if test="doc:HasForeignTax='true'"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td align="left"><xsl:value-of select="doc:Name"></xsl:value-of> </td><td align="right"><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:ForeignTax"></xsl:with-param></xsl:call-template> </td><td align="left"><xsl:value-of select="doc:FTCountryName"></xsl:value-of> </td></tr></xsl:if></xsl:for-each></table></xsl:if><xsl:if test="//doc:body/doc:Doh_KDVP/doc:KDVP/doc:IsResident='false'"><p> </p><h3>Znižanje oziroma oprostitev plačila dohodnine od dobička od odsvojitve
vrednostnih papirjev in drugih deležev ter investicijskih kuponov na podlagi
določb mednarodne pogodbe o izogibanju dvojnega obdavčevanja dohodka </h3><p>
        Podpisani uveljavljam znižanje oziroma oprostitev plačila dohodnine od prejetih dobičkov od odsvojitve vrednostnih papirjev in 
drugih deležev ter investicijskih kuponov na podlagi 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:RemissionState"></xsl:value-of></span>. 
                  odstavka 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:RemissionArticle"></xsl:value-of></span>. 
                  člena mednarodne pogodbe o izogibanju dvojnega obdavčevanja dohodka med Republiko Slovenijo in 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:CountryOfResidenceName"></xsl:value-of></span> 
                  in potrjujem, da:</p><p> </p><p>- sem rezident 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:CountryOfResidenceName"></xsl:value-of></span> 
                  v smislu določb mednarodne pogodbe o izogibanju dvojnega obdavčevanja med Republiko Slovenijo in 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:CountryOfResidenceName"></xsl:value-of></span>. 
                  </p><p></p><p> </p><p>Prilagam potrdilo o rezidentstvu, ki ga je izdal pristojni organ 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:ResConfirmationInstitution"></xsl:value-of></span> 
                  , z dne 
                  <span style="border: 1px solid #de9a8c;"><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:ResConfirmationDate"></xsl:with-param></xsl:call-template></span>, iz katerega je razvidno, da sem rezident 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:CountryOfResidenceName"></xsl:value-of></span> 
                  v smislu določb mednarodne pogodbe o
izogibanju dvojnega obdavčevanja dohodka med Republiko Slovenijo in 
                  <span style="border: 1px solid #de9a8c;"><xsl:value-of select="doc:CountryOfResidenceName"></xsl:value-of></span>. 
                  </p></xsl:if><xsl:if test="//doc:body/doc:Doh_KDVP/doc:Attachment"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td colspan="2">Priloge (popis dokumentov oziroma dokazil, ki jih zavezanec prilaga k napovedi): </td></tr><xsl:for-each select="//doc:body/doc:Doh_KDVP/doc:Attachment"><tr><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:value-of select="doc:Document"></xsl:value-of> </td></tr></xsl:for-each></table></xsl:if><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td>MF-DURS obr. DOHKAP št. 4</td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:for-each><xsl:for-each select="//doc:Doh_KDVP/doc:KDVPItem"><xsl:call-template name="Securities"></xsl:call-template><xsl:call-template name="SecuritiesShort"></xsl:call-template><xsl:call-template name="SecuritiesWithContract"></xsl:call-template><xsl:call-template name="SecuritiesWithContractShort"></xsl:call-template><xsl:call-template name="Shares"></xsl:call-template></xsl:for-each></xsl:for-each><xsl:for-each select="//doc:body/doc:AttachmentHash"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr><td class="lbl" align="left">Zgoščena vrednost (SHA-1 hash)</td><td align="left"><xsl:choose xmlns:var="urn:hermes-softlab-com-durs-variant"><xsl:when test="not(doc:Hash)"> 
      </xsl:when><xsl:when test="doc:Hash=''"> 
      </xsl:when><xsl:otherwise><xsl:value-of select="doc:Hash"></xsl:value-of></xsl:otherwise></xsl:choose></td></tr></table></xsl:for-each><xsl:call-template name="InternalAttachments"></xsl:call-template></div></div></xsl:for-each></xsl:template><xsl:template name="Securities"><xsl:for-each select="doc:Securities"><p> </p><xsl:if test="doc:Row"><hr></hr></xsl:if><h3><xsl:choose><xsl:when test="doc:IsFond='true'">Popisni list investicijskega kupona</xsl:when><xsl:otherwise>Popisni list vrednostnega papirja</xsl:otherwise></xsl:choose><font size="+1"> <xsl:value-of select="../doc:Name"></xsl:value-of></font></h3><xsl:if test="../doc:HasForeignTax='true'"><p>Davek plačan v tujini
        : <xsl:call-template name="Amount4"><xsl:with-param name="amount" select="../doc:ForeignTax"></xsl:with-param></xsl:call-template> EUR, Država: 
        <xsl:value-of select="../doc:FTCountryName"></xsl:value-of></p></xsl:if><p>Prenos izgube po tretjem odstavku 97. člena ZDoh-2
      : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:HasLossTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Odsvojitev deleža, ki je bil v tujini pridobljen z zamenjavo deleža skladno z Direktivo 90/434/EGS
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:ForeignTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Uveljavljanje oprostitve po 5. točki drugega odstavka 96. člena ZDoh-2
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:TaxDecreaseConformance"></xsl:with-param></xsl:call-template> 
      </p><xsl:if test="doc:Row"><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td> </td><td>Datum pridobitve</td><td>Način pridobitve*</td><td>Količina</td><td>Nabavna vrednost ob pridobitvi (na enoto)</td><td>Plačan davek na dediščine in darila</td><td>Datum odsvojitve</td><td>Količina odsvojenega v.p.</td><td>Zaloga v.p.</td><td>Vrednost ob odsvojitvi (na enoto)</td><td>Pravilo iz drugega odstavka v povezavi s petim odstavkom 97. člena ZDoh-2 **</td></tr><tr class="lblheader2"><td align="center"> </td><td align="center">1</td><td align="center">2</td><td align="center">3</td><td align="center">4</td><td align="center">5</td><td align="center">6</td><td align="center">7</td><td align="center">8</td><td align="center">9</td><td align="center">10</td></tr><xsl:for-each select="doc:Row"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Purchase/doc:F1"></xsl:with-param></xsl:call-template> </td><td><xsl:value-of select="doc:Purchase/doc:F2"></xsl:value-of> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F3"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F4"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F5"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Sale/doc:F6"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F7"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:F8"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F9"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="YesNo"><xsl:with-param name="val" select="doc:Sale/doc:F10"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table><p class="instruct">* 
        Način pridobitve
  <br></br><p>   A - vložek kapitala</p><p>   B - nakup</p><p>   C - povečanje kapitala družbe z lastnimi sredstvi zavezanca</p><p>   D - povečanje kapitala družbe iz sredstev družbe</p><p>   E – zamenjava kapitala ob statusnih spremembah družbe</p><p>   F – dedovanje</p><p>   G – darilo</p><p>   H – drugo</p><br></br></p><p class="instruct">** Izpolnjeni pogoji za zmanjšanje pozitivne davčne osnove po drugem odstavku v povezavi s petim odstavkom 97. člena ZDoh-2 (vpisuje se DA oziroma NE).</p><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td>MF-DURS obr. DOHKAP št. 4</td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:if></xsl:for-each></xsl:template><xsl:template name="SecuritiesShort"><xsl:for-each select="doc:SecuritiesShort"><p> </p><xsl:if test="doc:Row"><hr></hr></xsl:if><h3><xsl:choose><xsl:when test="doc:IsFond='true'">Popisni list investicijskega kupona za posle na kratko</xsl:when><xsl:otherwise>Popisni list vrednostnega papirja za posle na kratko</xsl:otherwise></xsl:choose><font size="+1"> <xsl:value-of select="../doc:Name"></xsl:value-of></font></h3><xsl:if test="../doc:HasForeignTax='true'"><p>Davek plačan v tujini
        : <xsl:call-template name="Amount4"><xsl:with-param name="amount" select="../doc:ForeignTax"></xsl:with-param></xsl:call-template> EUR, Država: 
        <xsl:value-of select="../doc:FTCountryName"></xsl:value-of></p></xsl:if><xsl:if test="doc:Row"><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td> </td><td>Datum odsvojitve</td><td>Količina odsvojenega v.p.</td><td>Vrednost ob odsvojitvi (na enoto)</td><td>Datum pridobitve</td><td>Način pridobitve*</td><td>Količina pridobljenega v.p.</td><td>Zaloga v.p.</td><td>Nabavna vrednost ob pridobitvi (na enoto)</td><td>Plačan davek na dediščine in darila</td></tr><tr class="lblheader2"><td align="center"> </td><td align="center">1</td><td align="center">2</td><td align="center">3</td><td align="center">4</td><td align="center">5</td><td align="center">6</td><td align="center">7</td><td align="center">8</td><td align="center">9</td></tr><xsl:for-each select="doc:Row"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Sale/doc:F6"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F7"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F9"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Purchase/doc:F1"></xsl:with-param></xsl:call-template> </td><td><xsl:value-of select="doc:Purchase/doc:F2"></xsl:value-of> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F3"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:F8"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F4"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F5"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table><p class="instruct">* 
        Način pridobitve
  <br></br><p>   A - nakup</p><p>   B - dedovanje</p><p>   C - darilo</p><p>   D - drugo</p><br></br></p><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td>MF-DURS obr. DOHKAP št. 4</td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:if></xsl:for-each></xsl:template><xsl:template name="SecuritiesWithContract"><xsl:for-each select="doc:SecuritiesWithContract"><p> </p><xsl:if test="doc:Row"><hr></hr></xsl:if><h3><xsl:choose><xsl:when test="doc:IsFond='true'">Popisni list investicijskega kupona, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju</xsl:when><xsl:otherwise>Popisni list vrednostnega papirja, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju</xsl:otherwise></xsl:choose><font size="+1"> <xsl:value-of select="../doc:Name"></xsl:value-of> </font></h3><xsl:if test="doc:StockExchangeName!=''"><p>Ime borznoposredniške družbe
        : <xsl:value-of select="doc:StockExchangeName"></xsl:value-of></p></xsl:if><xsl:if test="../doc:HasForeignTax='true'"><p>Davek plačan v tujini
        : <xsl:call-template name="Amount4"><xsl:with-param name="amount" select="../doc:ForeignTax"></xsl:with-param></xsl:call-template> EUR, Država: 
        <xsl:value-of select="../doc:FTCountryName"></xsl:value-of></p></xsl:if><p>Prenos izgube po tretjem odstavku 97. člena ZDoh-2
      : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:HasLossTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Odsvojitev deleža, ki je bil v tujini pridobljen z zamenjavo deleža skladno z Direktivo 90/434/EGS
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:ForeignTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Uveljavljanje oprostitve po 5. točki drugega odstavka 96. člena ZDoh-2
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:TaxDecreaseConformance"></xsl:with-param></xsl:call-template> 
      </p><xsl:if test="doc:Row"><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td> </td><td>Datum pridobitve</td><td>Način pridobitve*</td><td>Količina</td><td>Nabavna vrednost ob pridobitvi (na enoto)</td><td>Plačan davek na dediščine in darila</td><td>Datum odsvojitve</td><td>Količina odsvojenega v.p.</td><td>Zaloga v.p.</td><td>Vrednost ob odsvojitvi (na enoto)</td><td>Pravilo iz drugega odstavka v povezavi s petim odstavkom 97. člena ZDoh-2 **</td></tr><tr class="lblheader2"><td align="center"> </td><td align="center">1</td><td align="center">2</td><td align="center">3</td><td align="center">4</td><td align="center">5</td><td align="center">6</td><td align="center">7</td><td align="center">8</td><td align="center">9</td><td align="center">10</td></tr><xsl:for-each select="doc:Row"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Purchase/doc:F1"></xsl:with-param></xsl:call-template> </td><td><xsl:value-of select="doc:Purchase/doc:F2"></xsl:value-of> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F3"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F4"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F5"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Sale/doc:F6"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F7"></xsl:with-param></xsl:call-template>               
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:F8"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F9"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="YesNo"><xsl:with-param name="val" select="doc:Sale/doc:F10"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table><p class="instruct">* 
        Način pridobitve
  <br></br><p>   A - vložek kapitala</p><p>   B - nakup</p><p>   C - povečanje kapitala družbe z lastnimi sredstvi zavezanca</p><p>   D - povečanje kapitala družbe iz sredstev družbe</p><p>   E – zamenjava kapitala ob statusnih spremembah družbe</p><p>   F – dedovanje</p><p>   G – darilo</p><p>   H – drugo</p><br></br></p><p class="instruct">** Izpolnjeni pogoji za zmanjšanje pozitivne davčne osnove po drugem odstavku v povezavi s petim odstavkom 97. člena ZDoh-2 (vpisuje se DA oziroma NE).<br></br></p><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td>MF-DURS obr. DOHKAP št. 4</td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:if></xsl:for-each></xsl:template><xsl:template name="SecuritiesWithContractShort"><xsl:for-each select="doc:SecuritiesWithContractShort"><p> </p><xsl:if test="doc:Row"><hr></hr></xsl:if><h3><xsl:choose><xsl:when test="doc:IsFond='true'">Popisni list investicijskega kupona za posle na kratko, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju</xsl:when><xsl:otherwise>Popisni list vrednostnega papirja za posle na kratko, ki je v gospodarjenju pri BPD na podlagi pogodbe o gospodarjenju</xsl:otherwise></xsl:choose><font size="+1"> <xsl:value-of select="../doc:Name"></xsl:value-of> </font></h3><xsl:if test="doc:StockExchangeName!=''"><p>Ime borznoposredniške družbe
        : <xsl:value-of select="doc:StockExchangeName"></xsl:value-of></p></xsl:if><xsl:if test="../doc:HasForeignTax='true'"><p>Davek plačan v tujini
        : <xsl:call-template name="Amount4"><xsl:with-param name="amount" select="../doc:ForeignTax"></xsl:with-param></xsl:call-template> EUR, Država: 
        <xsl:value-of select="../doc:FTCountryName"></xsl:value-of></p></xsl:if><xsl:if test="doc:Row"><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td> </td><td>Datum odsvojitve</td><td>Količina odsvojenega v.p.</td><td>Vrednost ob odsvojitvi (na enoto)</td><td>Datum pridobitve</td><td>Način pridobitve*</td><td>Količina pridobljenega v.p.</td><td>Zaloga v.p.</td><td>Nabavna vrednost ob pridobitvi (na enoto)</td><td>Plačan davek na dediščine in darila</td></tr><tr class="lblheader2"><td align="center"> </td><td align="center">1</td><td align="center">2</td><td align="center">3</td><td align="center">4</td><td align="center">5</td><td align="center">6</td><td align="center">7</td><td align="center">8</td><td align="center">9</td></tr><xsl:for-each select="doc:Row"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Sale/doc:F6"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F7"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F9"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Purchase/doc:F1"></xsl:with-param></xsl:call-template> </td><td><xsl:value-of select="doc:Purchase/doc:F2"></xsl:value-of> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F3"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:F8"></xsl:with-param></xsl:call-template> 
              </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F4"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F5"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table><p class="instruct">* 
        Način pridobitve
  <br></br><p>   A - nakup</p><p>   B - dedovanje</p><p>   C - darilo</p><p>   D - drugo</p><br></br></p><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td>MF-DURS obr. DOHKAP št. 4</td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:if></xsl:for-each></xsl:template><xsl:template name="Shares"><xsl:for-each select="doc:Shares"><p> </p><xsl:if test="doc:Row"><hr></hr></xsl:if><h3>Popisni list deleža v gospodarskih družbah, zadrugah in drugih oblikah organiziranja<font size="+1"> <xsl:value-of select="../doc:Name"></xsl:value-of></font></h3><xsl:if test="doc:SubsequentPayments"><p>Uveljavljanje naknadnih vplačil v skladu s 4. točko sedmega odstavka 98. člena Zdoh-2
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="doc:SubsequentPayments"></xsl:with-param></xsl:call-template> 
        </p><xsl:if test="doc:SubsequentPaymentRow"><table class="tablesignbullet"><tr class="first"><td> </td><td>Datum naknadnega vplačila</td><td>Znesek</td></tr><xsl:for-each select="doc:SubsequentPaymentRow"><tr><td><xsl:value-of select="position()"></xsl:value-of> 
                </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:PaymentDate"></xsl:with-param></xsl:call-template> 
                </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:PaymentAmount"></xsl:with-param></xsl:call-template> 
                </td></tr></xsl:for-each></table></xsl:if></xsl:if><xsl:if test="../doc:HasForeignTax='true'"><p>Davek plačan v tujini
        : <xsl:call-template name="Amount4"><xsl:with-param name="amount" select="../doc:ForeignTax"></xsl:with-param></xsl:call-template> EUR, Država: 
        <xsl:value-of select="../doc:FTCountryName"></xsl:value-of></p></xsl:if><p>Prenos izgube po tretjem odstavku 97. člena ZDoh-2
      : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:HasLossTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Odsvojitev deleža, ki je bil v tujini pridobljen z zamenjavo deleža skladno z Direktivo 90/434/EGS
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:ForeignTransfer"></xsl:with-param></xsl:call-template> 
      </p><p>Uveljavljanje oprostitve po 5. točki drugega odstavka 96. člena ZDoh-2
        : <xsl:call-template name="YesNo"><xsl:with-param name="val" select="../doc:TaxDecreaseConformance"></xsl:with-param></xsl:call-template> 
      </p><xsl:if test="doc:Row"><table class="tablesignbullet" cellspacing="0" cellpadding="2"><tr class="first"><td> </td><td>Datum pridobitve</td><td>Način pridobitve*</td><td>Nabavna vrednost ob pridobitvi</td><td>Plačan davek na dediščine in darila</td><td>Datum odsvojitve</td><td>% odsvojenega deleža</td><td>Stanje deleža</td><td>Vrednost ob odsvojitvi</td><td>Pravilo iz drugega odstavka v povezavi s petim odstavkom 97. člena ZDoh-2 **</td></tr><tr class="lblheader2"><td align="center"> </td><td align="center">1</td><td align="center">2</td><td align="center">3</td><td align="center">4</td><td align="center">5</td><td align="center">6</td><td align="center">7</td><td align="center">8</td><td align="center">9</td></tr><xsl:for-each select="doc:Row"><tr align="right"><td align="center"><xsl:value-of select="position()"></xsl:value-of> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Purchase/doc:F1"></xsl:with-param></xsl:call-template> </td><td><xsl:value-of select="doc:Purchase/doc:F2"></xsl:value-of> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F3"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Purchase/doc:F4"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="doc:Sale/doc:F5"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F6"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:F7"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="Amount4"><xsl:with-param name="amount" select="doc:Sale/doc:F8"></xsl:with-param></xsl:call-template> </td><td><xsl:call-template name="YesNo"><xsl:with-param name="val" select="doc:Sale/doc:F9"></xsl:with-param></xsl:call-template> </td></tr></xsl:for-each></table><p class="instruct">* 
        Način pridobitve
  <br></br><p>    A - vložek kapitala</p><p>    B - nakup</p><p>    C – povečanje kapitala družbe z lastnimi sredstvi zavezanca</p><p>    D – povečanje kapitala družbe iz sredstev družbe</p><p>    E – zamenjava kapitala ob statusnih spremembah družbe</p><p>    F – dedovanje</p><p>    G - darilo</p><p>    H – drugo</p><p>    I – povečanje kapitalskega deleža v osebni družbi zaradi pripisa dobička kapitalskemu deležu</p><br></br></p><p class="instruct">** Izpolnjeni pogoji za zmanjšanje pozitivne davčne osnove po drugem odstavku v povezavi s petim odstavkom 97. člena ZDoh-2 (vpisuje se DA oziroma NE).</p><div style="display:inline-block; width:100%;" xmlns:var="urn:hermes-softlab-com-durs-variant"><table class="tablesignform" cellspacing="0"><tr><td></td><td class="right">e-Doh_KDVP_9.17</td></tr></table></div></xsl:if></xsl:for-each></xsl:template><xsl:template name="Amount"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="Amount1"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0.0'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="Amount2"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0.00'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="Amount3"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0.000'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="Amount4"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0.0000'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="Amount5"><xsl:param name="amount"></xsl:param><xsl:choose><xsl:when test="not($amount)"> </xsl:when><xsl:otherwise><xsl:value-of select="translate(format-number($amount, '#,##0.00000'), '.,', ',.')"></xsl:value-of></xsl:otherwise></xsl:choose></xsl:template><xsl:template name="dateAndTime"><xsl:param name="timestamp"></xsl:param><xsl:if test="not($timestamp='')"><xsl:call-template name="dateOnly"><xsl:with-param name="timestamp" select="$timestamp"></xsl:with-param></xsl:call-template> <xsl:value-of select="substring($timestamp, 12, 8)"></xsl:value-of></xsl:if></xsl:template><xsl:template name="dateOnly"><xsl:param name="timestamp"></xsl:param><xsl:variable name="tmonth"><xsl:value-of select="substring($timestamp, 6, 2)"></xsl:value-of></xsl:variable><xsl:variable name="tday"><xsl:value-of select="substring($timestamp, 9, 2)"></xsl:value-of></xsl:variable><xsl:variable name="tyear"><xsl:value-of select="substring($timestamp, 1, 4)"></xsl:value-of></xsl:variable><xsl:if test="($timestamp) and ($timestamp != '')"><xsl:value-of select="$tday"></xsl:value-of>.<xsl:value-of select="$tmonth"></xsl:value-of>.<xsl:value-of select="$tyear"></xsl:value-of></xsl:if></xsl:template><xsl:template name="InternalAttachments"><xsl:if test="//edp:AttachmentList/edp:ExternalAttachment"><p> </p><table class="tablesignbullet" cellspacing="0" cellpadding="2" width="100%"><tr class="first"><td colspan="6">Vsebovane priloge</td></tr><tr class="lblheader"><td>#</td><td>Id</td><td>Ime datoteke</td><td>Tip priloge</td><td>Opis</td><td>Zgoščena vrednost (SHA-1 hash)</td></tr><xsl:for-each select="//edp:AttachmentList/edp:ExternalAttachment"><tr><td class="left"><xsl:value-of select="position()"></xsl:value-of>.</td><td class="left"><xsl:value-of select="edp:attachmentId"></xsl:value-of></td><td class="left"><a><xsl:attribute name="href">../../CommonPages/Documents/AttachmentDownload.aspx?intAttId=<xsl:value-of select="edp:attachmentId"></xsl:value-of></xsl:attribute><xsl:value-of select="edp:filename"></xsl:value-of></a></td><td class="left"><xsl:value-of select="edp:type"></xsl:value-of></td><td class="left"><xsl:value-of select="edp:description"></xsl:value-of></td><td class="left"><xsl:value-of select="edp:hash"></xsl:value-of></td></tr></xsl:for-each></table></xsl:if></xsl:template><xsl:template name="YesNo"><xsl:param name="val"></xsl:param><xsl:choose><xsl:when test="translate($val,'TRUEFALS','truefals') = 'true'">Da</xsl:when><xsl:when test="translate($val,'TRUEFALS','truefals') = 'false'">Ne</xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template></xsl:stylesheet>