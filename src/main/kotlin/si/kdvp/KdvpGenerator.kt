package si.kdvp

import java.time.LocalDate

class KdvpGenerator {

    private fun createKdvpItem(
        inventoryType: TypeInventoryEnum,
        shareCode: String,
        closedPositionsForShare: ArrayList<ClosedPosition>,
        conversionsRateProvider: CurrencyConverter
    )
            : Doh_KDVP.KDVPItem {

        // closed positions from eToro have share name and code, while from
        // BKS they have only share code, so try to find the first one with
        // share name defined and use it.
        var shareName: String = shareCode
        for (closedPos in closedPositionsForShare) {
            if (closedPos.shareName.isNotEmpty()) {
                shareName = closedPos.shareName
                break;
            }
        }

        val kdvpItem = Doh_KDVP.KDVPItem(inventoryListType = inventoryType, name = shareName)

        kdvpItem.securities = createSecurity(shareName,
                                             shareCode,
            false,
            closedPositionsForShare,
            conversionsRateProvider)
        return kdvpItem
    }


    // TODO this is copy-pasted from EToroConverter. Move to common class.
    private fun createSecurity(shareName: String,
                               shareCode: String,
                               isFond: Boolean,
                               closedPositionsList: ArrayList<ClosedPosition>,
                               currencyConverter: CurrencyConverter): Securities {

        val rows = ArrayList<Securities.Row>()

        for (closedPosition in closedPositionsList) {
            if (closedPosition.type != EPositionType.Stocks) {
                continue
            }
            val purchaseRow = Securities.Row()
            // closedPosition.id is not useful, since it is the same for purchase and sale
            val purchase = Securities.Row.Purchase()
            val openConvRate = currencyConverter.getConversionRate(closedPosition.openDate,
                                                                   closedPosition.currency)
            purchase.f1 = closedPosition.openDate // .format(DateTimeFormatter.ISO_DATE)
            purchase.f2 = TypeGainTypeEnum.B  // nakup
            purchase.f3 = closedPosition.units  // Količina
            purchase.f4 = closedPosition.openRate / openConvRate // Nabavna vrednost ob pridobitvi (na enoto)
            purchaseRow.purchase = purchase
            rows.add(purchaseRow)

            val saleRow = Securities.Row()
            val sale = Securities.Row.Sale()
            sale.f6 = closedPosition.closeDate
            sale.f7 = closedPosition.units  // Količina odsvojenega v.p.
            val closeConvRate = currencyConverter.getConversionRate(closedPosition.closeDate,
                                                                    closedPosition.currency)
            sale.f9 = closedPosition.closeRate / closeConvRate // Vrednost ob osvojitvi (na enoto)

            // not present in masbug version, and this should be calculated by FURS, so don't write it
            sale.f10 = null // false // Pravilo iz  drugega odstavka v povezavi s petim odstavkom 97.člena ZDoh-2
            saleRow.sale = sale
            rows.add(saleRow)
        }

        rows.sortWith(RowComparator())

        // assign IDs
        var idx = 0
        rows.forEach { it.id = idx++ }

        val security = Securities(code = shareCode, name = shareName, isFond = isFond)
        security.rows = rows

        return security
    }


    fun createEnvelope(envelopeWithTaxpayer: Envelope,
                       closedPositionsMap: ClosedPositionsMap,
                       currencyConverter: CurrencyConverter,
                       taxYear: Int): Envelope {
        val outputEnvelope = Envelope()
        outputEnvelope.header.taxpayer = envelopeWithTaxpayer.header.taxpayer

        // dates could also be set here, but it is recommended to make
        // new export with current dates every year just in case some data changes.
        outputEnvelope.body.doh_KDVP.kdvp =
            envelopeWithTaxpayer.body.doh_KDVP.kdvp.copy(
                securityCount = closedPositionsMap.size,
                securityShortCount = 0
            )
        outputEnvelope.body.doh_KDVP.kdvpItems = ArrayList<Doh_KDVP.KDVPItem>()
        for (closedPosition in closedPositionsMap) {
            val kdvpItem = createKdvpItem(
                    TypeInventoryEnum.PLVP,
                    closedPosition.key,
                    closedPosition.value,
                    currencyConverter
                )
            if (kdvpItem.securities?.rows?.isNotEmpty() == true) {   // ETFs have this empty
                outputEnvelope.body.doh_KDVP.kdvpItems!!.add(kdvpItem)
            }
        }

        return outputEnvelope
    }
}


class RowComparator : Comparator<Securities.Row>
{
    fun getRowDate(row: Securities.Row): LocalDate
    {
        val purchase = row.purchase
        val sale = row.sale

        val rowDate = when {
            purchase != null -> purchase.f1
            sale != null -> sale.f6
            else ->
                throw IllegalArgumentException("Sale and Purchase must not be both null: {row}")
        }

        rowDate ?: throw IllegalArgumentException("Date in row must not be null: {row}")

        return rowDate
    }

    override fun compare(left: Securities.Row?, right: Securities.Row?): Int {
        left ?: throw IllegalArgumentException("Row must not be null: {left}")
        right ?: throw IllegalArgumentException("Row must not be null: {right}")
        val leftDate = getRowDate(left)
        val rightDate = getRowDate(right)
        val result = leftDate.compareTo(rightDate)

        // Make sure purchase is shown before sale on the same date. It may happen,
        // that sale occurs before purchase on th same day, but this is not a problem,
        // because of FIFO rule.
        if (result == 0) {
            if (left.purchase != null  &&  right.sale != null) {
                return -1
            }
            if (left.sale != null  &&  right.purchase != null) {
                return 1
            }
        }
        return result
    }
}
