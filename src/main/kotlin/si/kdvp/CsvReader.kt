package si.kdvp

import java.io.File
import java.lang.IllegalStateException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.math.absoluteValue

enum class CsvColumn(val colIdx: Int) {
    VP(0),
    `Datum valute`(1),
    `Tip transakcije`(2),
    Količina(4),
    Tečaj(5),
    `Št naročila`(7)
}

enum class DenarColumn(val colIdx: Int) {
    Valuta(0),
    `Tip transakcije`(2),
    `Št naročila`(7)
}

data class CsvTransaction(val securityCode: String,
                          val date: LocalDate,
                          val transactionType: EAction,
                          val numSecurities: Int,
                          val rate: Double,
                          var remaining: Int,  // used during FIFO counting
                          val positionInfo: PositionInfo)

class CsvConverter {

    private fun toDouble(columns: List<String>, colIdx: CsvColumn): Double {
        return columns[colIdx.colIdx].
            trim().
            replace(".", "").
            replace(',', '.').
            toDouble()
    }


    private fun toStr(columns: List<String>, colIdx: Int): String {
        return columns[colIdx].trim()
    }

    private fun toLong(columns: List<String>, colIdx: Int): Long {
        return columns[colIdx].trim().toLong()
    }

    private val NUM_VP_COLUMNS: Int = 8
    private val NUM_DENAR_COLUMNS: Int = 8

    private fun readCsv(fileName: String): List<CsvTransaction> {

        val positionInfoMap = readPositionInfo("$fileName-denar")

        val inf = File(fileName)
        val lines = inf.readLines()
        val header = Array<String>(NUM_VP_COLUMNS) {""}
        val transactions = ArrayList<CsvTransaction>()
        val dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy")

        for ((idx, line) in lines.withIndex()) {
            if (idx < NUM_VP_COLUMNS) {
                header[idx] = line.trim()
                continue
            }

            val columns = line.split(";")

            val transactionStr = toStr(columns, CsvColumn.`Tip transakcije`.colIdx)
            val transactionType = when(transactionStr) {
                "Nakup" -> EAction.Buy
                "Prodaja" -> EAction.Sell
                else ->
                    throw IllegalArgumentException("Napačna transakcija: $transactionStr")
            }

            val numSecuritiesDbl = toDouble(columns, CsvColumn.Količina)
            val numSecuritiesInt = numSecuritiesDbl.toInt()
            if (numSecuritiesInt.toDouble() != numSecuritiesDbl) {
                throw IllegalArgumentException("Number of securities is not an integer, " +
                                                       "as expected: $numSecuritiesDbl")
            }

            val positionId = toLong(columns, CsvColumn.`Št naročila`.colIdx)
            val positionInfo = positionInfoMap[positionId]
            positionInfo ?: throw IllegalStateException("Position info not found in table 'Denar', " +
                                                        "positionId: $positionId")

            // split 'TSLA US' to ['TSLA', 'US']
            val shareId = toStr(columns, CsvColumn.VP.colIdx).split(' ')[0]
            transactions.add(CsvTransaction(shareId,
                                            LocalDate.parse(toStr(columns, CsvColumn.`Datum valute`.colIdx),
                                                            dateFormat),
                                            transactionType,
                                            numSecuritiesInt,
                                            toDouble(columns, CsvColumn.Tečaj),
                                            numSecuritiesInt,
                                            positionInfo))
        }
        return transactions
    }

    private fun transactionsToClosedPosMap(transactions: List<CsvTransaction>,
                                           taxYear: Int): ClosedPositionsMap {

        // copy transaction to map with share name as a key
        val transactionsMap: MutableMap<String, MutableList<CsvTransaction>> = HashMap()
        for (t in transactions) {
            val securityId = t.securityCode
            if (!transactionsMap.containsKey(securityId)) {
                transactionsMap[securityId] = ArrayList()
            }
            transactionsMap[securityId]?.add(t)
        }

        val closedPositionsMap = LinkedHashMap<String, ArrayList<ClosedPosition>>()
        for (entry in transactionsMap) {
            // sort transactions by date for each share. Important for FIFO rule.
            entry.value.sortWith(CsvTransComparator())
            val closedPosForShare = transactionsToClosedPositions(entry.value, taxYear)
            if (closedPosForShare.size > 0) {
                closedPositionsMap[entry.key] = closedPosForShare
            }
        }

        return closedPositionsMap
    }

    private fun transactionsToClosedPositions(transList: List<CsvTransaction>,
                                              taxYear: Int): ArrayList<ClosedPosition> {
        var supplyOfShares: Int = 0  // used for diagnostic and verification only
        val closedPositionsList = ArrayList<ClosedPosition>();
        for ((idx, transForShare) in transList.withIndex()) {
            when (transForShare.transactionType) {
                EAction.Buy -> supplyOfShares += transForShare.numSecurities
                EAction.Sell -> {
                    var numToSell = transForShare.numSecurities.absoluteValue // sold numbers are negative in csv
                    for ((buyIdx, transForShareRemaining) in transList.withIndex()) {
                        if (buyIdx >= idx) {
                            throw IllegalStateException("Bug: Sold more shares than bought: $transForShare")
                        }
                        if (transForShareRemaining.transactionType == EAction.Buy) {
                            if (transForShareRemaining.remaining >= numToSell) {

                                transForShareRemaining.remaining -= numToSell
                                addClosedPosition(transForShareRemaining, transForShare,
                                                  numToSell, taxYear, closedPositionsList)
                                numToSell = 0
                                break;
                            } else {
                                numToSell -= transForShareRemaining.remaining
                                addClosedPosition(transForShareRemaining, transForShare,
                                                  transForShareRemaining.remaining, taxYear,
                                                  closedPositionsList)
                                transForShareRemaining.remaining = 0
                            }
                        }
                    }

                    if (numToSell > 0) {
                        throw IllegalStateException("More shares sold than available: $transForShare")
                    }
                    supplyOfShares -= transForShare.numSecurities.absoluteValue
                }
            }
        }
        return closedPositionsList
    }


    private fun addClosedPosition(transBuy: CsvTransaction,
                                  transSell: CsvTransaction,
                                  numSold: Int,
                                  taxYear: Int,
                                  closedPositionsList: ArrayList<ClosedPosition>) {
        if (transSell.date.year != taxYear) {
            // skip in report
            return
        }

        val closedPosition = ClosedPosition(closedPositionsList.size.toLong(),
                                            EAction.Buy,
                                            transBuy.securityCode,
                                            "",  // BKS provides only share code, ag. AMZN
                                            0.0,  // not needed for KDVP
                                            numSold.toDouble(),
                                            transBuy.rate,
                                            transSell.rate,
                                            0.0,
                                            transBuy.date,
                                            transSell.date,
                                            EPositionType.Stocks,
                                            transSell.positionInfo.currency)
        closedPositionsList.add(closedPosition)
    }

    private fun readPositionInfo(fileName: String): PositionInfoMap {
        val inf = File(fileName)
        val lines = inf.readLines()
        val header = Array<String>(NUM_DENAR_COLUMNS) {""}

        val positionInfoMap = PositionInfoMap()

        for ((idx, line) in lines.withIndex()) {
            if (idx < NUM_DENAR_COLUMNS) {
                header[idx] = line.trim()
                continue
            }

            val columns = line.split(";")

            val currency = Currency.valueOf(toStr(columns, DenarColumn.Valuta.colIdx))
            val transTypeItems = toStr(columns, DenarColumn.`Tip transakcije`.colIdx).split(' ')
            // Example: "Prodaja TSLA US"
            if (transTypeItems.size == 3  &&
                (transTypeItems[0] == "Prodaja" || transTypeItems[0] == "Nakup")) {

                val shareId = transTypeItems[1]
                // positionId is not present in all types of rows
                val positionId = toLong(columns, DenarColumn.`Št naročila`.colIdx)
                positionInfoMap[positionId] = PositionInfo(shareId, currency)
            }
        }

        return positionInfoMap
    }

    fun convert(csvFile: String, taxYear: Int): ClosedPositionsMap {
        val transactions = readCsv(csvFile)
        return transactionsToClosedPosMap(transactions, taxYear)
    }
}


class CsvTransComparator : Comparator<CsvTransaction>
{
    override fun compare(left: CsvTransaction, right: CsvTransaction): Int {
        return left.date.compareTo(right.date)
    }
}
