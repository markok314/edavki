package si.kdvp

/*
This file was generated using schema-gen, but then manually edited,
because schema-gen has some bugs and missing functionality.

Classes here represent XML schema Doh_KDVP_9.xsd.
 */


import com.fasterxml.jackson.annotation.*
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement

import javax.validation.Valid
import javax.validation.constraints.DecimalMax
import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Digits
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern

enum class TaxpayerTypeTypeEnum(@JsonValue val value:String)
{
    FO("FO"),
    PO("PO"),
    SP("SP")
}

data class Receipt(
    var timestamp:java.time.LocalDateTime = java.time.LocalDateTime.now(),
    var documentNumber:String = "",
    var name:String? = null,
    var metaText1:String? = null,
    var metaDate1:java.time.LocalDateTime? = java.time.LocalDateTime.now()
)
{
}

data class Signer(
    var timestamp:java.time.LocalDateTime? = java.time.LocalDateTime.now(),
    var name:String = ""
)
{
}

data class ServerSigner(
    var timestamp:java.time.LocalDateTime? = java.time.LocalDateTime.now(),
    var name:String = "",
    var id:String = ""
)
{
}

data class Presentation(
    @field:Valid var map:Map_? = null
)
{
}

@JsonPropertyOrder(value = ["taxNumber", "taxpayerType", "name", "address1", "address2", "city",
"postNumber", "postName", "municipalityName", "birthDate", "maticnaStevilka", "invalidskoPodjetje",
"resident", "activityCode", "activityName", "countryId", "countryName"])
data class TaxPayer(
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var taxNumber: Int? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") @field:Valid var taxpayerType:TaxpayerTypeTypeEnum? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var name:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var address1:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var address2:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var city:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var postNumber:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var postName:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var municipalityName:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var birthDate:java.time.LocalDate? = java.time.LocalDate.now(),
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var maticnaStevilka:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var invalidskoPodjetje:Boolean? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var resident:Boolean? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var activityCode:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var activityName:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var countryID:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var countryName:String? = null
)
{
}

data class Workflow(
    var documentWorkflowID:String? = null,
    var documentWorkflowName:String? = null
)
{
}

data class Custodian(
    var name:String? = null,
    var address1:String? = null,
    var address2:String? = null,
    var city:String? = null,
    var custodianNotes:String? = null,
    var custodianSubmitDate:java.time.LocalDate? = java.time.LocalDate.now()
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class Header(
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") @field:Valid var taxpayer:TaxPayer = TaxPayer(),
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var responseTo:String? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd",
    localName = "Workflow") @field:Valid var workflow:Workflow? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd",
    localName = "CustodianInfo") @field:Valid var custodianInfo:Custodian? = null,
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd") var domain:String? = null
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class Transformation(
    var value:String? = null
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class Signatures(
    var value:PreparerSignature? = null
)
{

    data class PreparerSignature(
        @field:Valid var preparer:Signer = Signer(),
        var any:String = ""
    )
    {
    }
    data class DepositorSignature(
        @field:Valid var depositor:Signer = Signer(),
        var any:String = ""
    )
    {
    }
    data class NonEDP(
        @field:Valid var receipt:Receipt = Receipt()
    )
    {
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class DepositorServerSignature(
    @field:Valid var depositor:ServerSigner = ServerSigner(),
    @field:Valid var receipt:Receipt = Receipt(),
    var any:String = ""
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class PreparerServerSignature(
    @field:Valid var preparer:ServerSigner = ServerSigner(),
    var any:String = ""
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class ServerSignature(
    @field:Valid var receipt:Receipt = Receipt(),
    var any:String = ""
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class TSASignature(
    var any:String = ""
)
{
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class NER(
    @get:Pattern(regexp="^\\d{1,14}$") var total_F8:Double? = 0.0,
    @get:Pattern(regexp="^\\d{1,14}$") var total_F9:Double? = 0.0,
    @get:Pattern(regexp="^\\d{1,14}$") var total_F10:Double? = 0.0,
    @JacksonXmlElementWrapper(localName="NERItems", useWrapping=false) @JacksonXmlProperty(localName="NERItem") @field:Valid var NERItems:MutableList<NERItem>? = mutableListOf()
)
{

    data class NERItem(
        var f1_Name:String? = null,
        var f2_Address:String? = null,
        var f3_ResidentCountry:String? = null,
        var f4_TaxNumber:Int? = 0,
        var f5_BirthDate:java.time.LocalDate? = java.time.LocalDate.now(),
        var f6_Benefits:String? = null,
        var f7_OtherBenefits:String? = null,
        @get:Pattern(regexp="^\\d{1,14}$") var f8:Double? = 0.0,
        @get:Pattern(regexp="^\\d{1,14}$") var f9:Double? = 0.0,
        @get:Pattern(regexp="^\\d{1,14}$") var f10:Double? = 0.0
    )
    {
    }
}

enum class TypeInventoryEnum(@JsonValue val value:String)
{
    PLD("PLD"),  // Popisni list deleža v gospodarskih družbah, zadrugah in drugih oblikah organiziranja
    PLVP("PLVP"),  // Popisni list vrednostnega papirja oziroma invecticijskega kupona
    PLVPSHORT("PLVPSHORT"),  // Popisni list vrednostnega papirja oziroma invecticijskega kupona SHORT
    PLVPGB("PLVPGB"),  // Popisni list vrednostnega papirja, ki je v gospodarjenju pri borznoposredniški družbi na podlagi pogodbe o gospodarjenju
    PLVPGBSHORT("PLVPGBSHORT"),  // Popisni list vrednostnega papirja, ki je v gospodarjenju pri borznoposredniški družbi na podlagi pogodbe o gospodarjenju SHORT
    Unknown("Unknown")
}

enum class TypeGainTypeEnum(@JsonValue val value:String)
{
    A("A"),
    B("B"),
    C("C"),
    D("D"),
    E("E"),
    F("F"),
    G("G"),
    H("H"),
    I("I")
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class Securities(
    @JacksonXmlProperty(localName="ISIN") var isin:String? = null,
    @JacksonXmlProperty(localName="Code") var code:String? = null,
    @JacksonXmlProperty(localName="Name") var name:String? = null,
    @JacksonXmlProperty(localName="IsFond") var isFond:Boolean = false,
    @JacksonXmlElementWrapper(localName="Rows", useWrapping=false) @JacksonXmlProperty(localName="Row") @field:Valid var rows:MutableList<Row>? = mutableListOf()
)
{

    @JsonPropertyOrder(value = ["ID", "Purchase", "Sale", "F8"])
    data class Row(
        @JacksonXmlProperty(localName="ID") @get:Min(0) var id:Int = 0,
        @JacksonXmlProperty(localName="F8")
        @JsonSerialize(using = DoubleSerializer::class)
        @get:Digits(integer=8,fraction=4)
        var f8:Double? = null,
        @JacksonXmlProperty(localName="Purchase") var purchase:Purchase? = null,
        @JacksonXmlProperty(localName="Sale") var sale:Sale? = null
    )
    {

        data class Purchase(
            @JacksonXmlProperty(localName="F1") var f1:java.time.LocalDate? = java.time.LocalDate.now(),
            @JacksonXmlProperty(localName="F2") @field:Valid var f2:TypeGainTypeEnum? = null,
            @JacksonXmlProperty(localName="F3")
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4)
            var f3:Double = 0.0,
            @JsonSerialize(using = DoubleSerializer::class)
            @JacksonXmlProperty(localName="F4") @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f4:Double? = 0.0,
            @JsonSerialize(using = DoubleSerializer::class)
            @JacksonXmlProperty(localName="F5") @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f5:Double? = 0.0
        )
        {
        }
        data class Sale(
            @JacksonXmlProperty(localName="F6") var f6:java.time.LocalDate? = java.time.LocalDate.now(),
            @JacksonXmlProperty(localName="F7")
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8, fraction=4)
            var f7:Double = 0.0,

            @JacksonXmlProperty(localName="F9")
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$")
            var f9:Double? = 0.0,

            @JacksonXmlProperty(localName="F10")
            var f10:Boolean? = null
        )
        {
        }
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class SecuritiesShort(
    @JacksonXmlProperty(localName="ISIN") var isin:String? = null,
    @JacksonXmlProperty(localName="Code") var code:String? = null,
    @JacksonXmlProperty(localName="Name") var name:String? = null,
    @JacksonXmlProperty(localName="IsFond") var isFond:Boolean = false,
    @JacksonXmlElementWrapper(localName="Rows", useWrapping=false) @JacksonXmlProperty(localName="Row") @field:Valid var Rows:MutableList<Row>? = mutableListOf()
)
{

    data class Row(
        @get:Min(0) var iD:Int = 0,
        @JsonSerialize(using = DoubleSerializer::class)
        @get:Digits(integer=10,fraction=4) var f8:Double? = 0.0,
        var value:String = ""
    )
    {

        data class Purchase(
            var f1:java.time.LocalDate? = java.time.LocalDate.now(),
            @field:Valid var f2:TypeGainTypeEnum? = null,
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f3:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f4:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f5:Double? = 0.0
        )
        {
        }
        data class Sale(
            var f6:java.time.LocalDate? = java.time.LocalDate.now(),
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f7:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f9:Double? = 0.0
        )
        {
        }
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class SecuritiesWithContract(
    @JacksonXmlProperty(localName="ISIN") var isin:String? = null,
    @JacksonXmlProperty(localName="Code") var code:String? = null,
    @JacksonXmlProperty(localName="Name") var name:String? = null,
    @JacksonXmlProperty(localName="IsFond") var isFond:Boolean = false,
    @JacksonXmlProperty(localName="StockExchangeName") var stockExchangeName:String? = null,
    @JacksonXmlElementWrapper(localName="Rows", useWrapping=false) @JacksonXmlProperty(localName="Row") @field:Valid var Rows:MutableList<Row>? = mutableListOf()
)
{

    data class Row(
        @get:Min(0) var iD:Int = 0,
        @JsonSerialize(using = DoubleSerializer::class)
        @get:Digits(integer=8,fraction=4) var f8:Double? = 0.0,
        var value:String = ""
    )
    {

        data class Purchase(
            var f1:java.time.LocalDate? = java.time.LocalDate.now(),
            @field:Valid var f2:TypeGainTypeEnum? = null,
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f3:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f4:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f5:Double? = 0.0
        )
        {
        }
        data class Sale(
            var f6:java.time.LocalDate? = java.time.LocalDate.now(),
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f7:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f9:Double? = 0.0,
            var f10:Boolean? = false
        )
        {
        }
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class SecuritiesWithContractShort(
    @JacksonXmlProperty(localName="ISIN") var isin:String? = null,
    @JacksonXmlProperty(localName="Code") var code:String? = null,
    @JacksonXmlProperty(localName="Name") var name:String? = null,
    @JacksonXmlProperty(localName="IsFond") var isFond:Boolean = false,
    @JacksonXmlProperty(localName="StockExchangeName") var stockExchangeName:String? = null,
    @JacksonXmlElementWrapper(localName="Rows", useWrapping=false) @JacksonXmlProperty(localName="Row") @field:Valid var Rows:MutableList<Row>? = mutableListOf()
)
{

    data class Row(
        @get:Min(0) var iD:Int = 0,
        @JsonSerialize(using = DoubleSerializer::class)
        @get:Digits(integer=10,fraction=4) var f8:Double? = 0.0,
        var value:String = ""
    )
    {

        data class Purchase(
            var f1:java.time.LocalDate? = java.time.LocalDate.now(),
            @field:Valid var f2:TypeGainTypeEnum? = null,
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f3:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f4:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f5:Double? = 0.0
        )
        {
        }
        data class Sale(
            var f6:java.time.LocalDate? = java.time.LocalDate.now(),
            @JsonSerialize(using = DoubleSerializer::class)
            @get:Digits(integer=8,fraction=4) var f7:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f9:Double? = 0.0
        )
        {
        }
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class Shares(
    var name:String? = null,
    var subsequentPayments:Boolean? = false,
    @JacksonXmlElementWrapper(localName="SubsequentPaymentRows", useWrapping=false) @JacksonXmlProperty(localName="SubsequentPaymentRow") @field:Valid var SubsequentPaymentRows:MutableList<SubsequentPaymentRow>? = mutableListOf(),
    @JacksonXmlElementWrapper(localName="Rows", useWrapping=false) @JacksonXmlProperty(localName="Row") @field:Valid var Rows:MutableList<Row>? = mutableListOf()
)
{

    data class SubsequentPaymentRow(
        var paymentDate:java.time.LocalDate? = java.time.LocalDate.now(),
        @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var paymentAmount:Double? = 0.0
    )
    {
    }
    data class Row(
        @get:Min(0) var iD:Int = 0,
        @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f7:Double? = 0.0,
        var value:String = ""
    )
    {

        data class Purchase(
            var f1:java.time.LocalDate? = java.time.LocalDate.now(),
            @field:Valid var f2:TypeGainTypeEnum? = null,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f3:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f4:Double? = 0.0
        )
        {
        }
        data class Sale(
            var f5:java.time.LocalDate? = java.time.LocalDate.now(),
            @get:Pattern(regexp="^\\d{1,3}(\\.\\d{1,4})?$") @get:DecimalMin("0") @get:DecimalMax("100") var f6:Double? = 0.0,
            @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var f8:Double? = 0.0,
            var f9:Boolean? = false
        )
        {
        }
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
@JsonPropertyOrder(value = ["KDVP","KDVPItem"])
data class Doh_KDVP(
    @JacksonXmlProperty(localName="KDVP") @field:Valid var kdvp:KDVP = KDVP(),
    @JacksonXmlElementWrapper(localName="TaxRelieves", useWrapping=false) @JacksonXmlProperty(localName="TaxRelief") @field:Valid var taxRelieves:MutableList<TaxRelief>? = mutableListOf(),
    @JacksonXmlElementWrapper(localName="TaxBaseDecreases", useWrapping=false) @JacksonXmlProperty(localName="TaxBaseDecrease") @field:Valid var taxBaseDecreases:MutableList<TaxBaseDecrease>? = mutableListOf(),
    @JacksonXmlElementWrapper(localName="Attachments", useWrapping=false) @JacksonXmlProperty(localName="Attachment") @field:Valid var attachments:MutableList<Attachment>? = mutableListOf(),
    @JacksonXmlElementWrapper(localName="KDVPItems", useWrapping=false)
    @JacksonXmlProperty(localName="KDVPItem")
    @field:Valid
    var kdvpItems:MutableList<KDVPItem>? = mutableListOf()
)
{
    data class KDVP(
        @JacksonXmlProperty(localName="DocumentWorkflowID") var documentWorkflowID:String? = null,
        @JacksonXmlProperty(localName="DocumentWorkflowName") var documentWorkflowName:String? = null,
        @JacksonXmlProperty(localName="Year") @get:Pattern(regexp="^[1-9][0-9]{3}$") var year:Int? = 0,
        @JacksonXmlProperty(localName="PeriodStart") var periodStart:java.time.LocalDate? = java.time.LocalDate.now(),
        @JacksonXmlProperty(localName="PeriodEnd") var periodEnd:java.time.LocalDate? = java.time.LocalDate.now(),
        @JacksonXmlProperty(localName="IsResident") var isResident:Boolean? = false,
        @JacksonXmlProperty(localName="CountryOfResidenceID") @get:Pattern(regexp="^[0-9]{3}$") var countryOfResidenceID:String? = null,
        @JacksonXmlProperty(localName="CountryOfResidenceName") var countryOfResidenceName:String? = null,
        @JacksonXmlProperty(localName="TelephoneNumber") var telephoneNumber:String? = null,
        @JacksonXmlProperty(localName="SecurityCount") @get:Min(0) var securityCount:Int = 0,
        @JacksonXmlProperty(localName="SecurityShortCount") @get:Min(0) var securityShortCount:Int = 0,
        @JacksonXmlProperty(localName="SecurityWithContractCount") @get:Min(0) var securityWithContractCount:Int = 0,
        @JacksonXmlProperty(localName="SecurityWithContractShortCount") @get:Min(0) var securityWithContractShortCount:Int = 0,
        @JacksonXmlProperty(localName="ShareCount") @get:Min(0) var shareCount:Int = 0,
        @JacksonXmlProperty(localName="RemissionState") var remissionState:String? = null,
        @JacksonXmlProperty(localName="RemissionArticle") var remissionArticle:String? = null,
        @JacksonXmlProperty(localName="ResConfirmationInstitution") var resConfirmationInstitution:String? = null,
        @JacksonXmlProperty(localName="ResConfirmationDate") var resConfirmationDate:java.time.LocalDate? = null,
        @JacksonXmlProperty(localName="Email") var email:String? = null
    )
    {
    }
    data class TaxRelief(
        var orderNumber:String? = null,
        var orderDate:java.time.LocalDate? = java.time.LocalDate.now(),
        var acquirementDate:java.time.LocalDate? = java.time.LocalDate.now(),
        var expropriationDate:java.time.LocalDate? = java.time.LocalDate.now(),
        @get:Pattern(regexp="^\\d{1,12}(\\.\\d{1,2})?$") var loss:Double? = 0.0,
        @get:Pattern(regexp="^\\d{1,12}(\\.\\d{1,2})?$") var profit:Double? = 0.0,
        @get:Pattern(regexp="^\\d{1,12}(\\.\\d{1,2})?$") var incomeTax:Double? = 0.0,
        var isPrefilled:Boolean? = false
    )
    {
    }
    data class TaxBaseDecrease(
        var orderNumber:String? = null,
        var orderDate:java.time.LocalDate? = java.time.LocalDate.now(),
        @get:Pattern(regexp="^[0-9]{2}$") var taxOfficeID:String? = null,
        var taxOfficeName:String? = null,
        @get:Pattern(regexp="^\\d{1,12}(\\.\\d{1,2})?$") var loss:Double? = 0.0
    )
    {
    }
    data class Attachment(
        var document:String? = null
    )
    {
    }
    data class KDVPItem(
        @JacksonXmlProperty(localName = "InventoryListType") @field:Valid var inventoryListType:TypeInventoryEnum = TypeInventoryEnum.Unknown,
        @JacksonXmlProperty(localName = "Name") var name:String? = null,
        @JacksonXmlProperty(localName = "HasForeignTax") var hasForeignTax:Boolean? = false,
        @JacksonXmlProperty(localName = "ForeignTax") @get:Pattern(regexp="^\\d{1,10}(\\.\\d{1,4})?$") var foreignTax:Double? = null,
        @JacksonXmlProperty(localName = "FTCountryID") @get:Pattern(regexp="^[0-9]{3}$") var ftCountryId:String? = null,
        @JacksonXmlProperty(localName = "FTCountryName") var ftCountryName:String? = null,
        @JacksonXmlProperty(localName = "HasLossTransfer") var hasLossTransfer:Boolean? = false,
        @JacksonXmlProperty(localName = "ForeignTransfer") var foreignTransfer:Boolean? = false,
        @JacksonXmlProperty(localName = "TaxDecreaseConformance") var taxDecreaseConformance:Boolean? = false,
        @JacksonXmlProperty(localName = "Securities") var securities:Securities? = null,
        @JacksonXmlProperty(localName = "SecuritiesShort") var securitiesShort:SecuritiesShort? = null
    )
    {
    }
}

@JsonIgnoreProperties(value = ["schemaLocation"])
data class AttachmentHash(
    var hash:String? = null
)
{
}

// xmlns:edp="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd"
@JacksonXmlRootElement(namespace="http://edavki.durs.si/Documents/Schemas/Doh_KDVP_9.xsd")
//@JsonIgnoreProperties(value = ["schemaLocation"])
data class Envelope(
    // this is a workaround for the second namespace, if needed
    // @JacksonXmlProperty(isAttribute = true, localName = "xmlns:edp")
    // var xmlns: String = "http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd",

    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd", localName="Header")
    var header:Header = Header(),
    @JacksonXmlElementWrapper(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd", localName="AttachmentList")
    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd", localName="AttachmentList")
    var attachmentList:MutableList<ExternalAttachment>? = mutableListOf(),

    @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd",
                        localName="Signatures")
    var signatures:Signatures? = Signatures(),

    @JacksonXmlProperty()
    var body:Body = Body()
)
{
    data class ExternalAttachment(
        var attachmentId:Int? = 0,
        var type:String? = null,
        var filename:String = "",
        var mimetype:String = "",
//        @field:Valid var hash:Hash = Hash(),
        var description:String? = null
    )
    {
/*  has problem with deserialization, but it is not used in my case
        data class Hash(
            @JacksonXmlProperty(localName="type", isAttribute = true) var type:String = "",
            @JacksonXmlText var text:String? = null
        )
        {
            constructor(text_:String): this(text=text_) {
            }
        }
*/
    }
    data class Body(
        @JacksonXmlProperty(namespace="http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd")
        var bodyContent:String? = "",

        @JacksonXmlProperty(localName="Doh_KDVP")
        var doh_KDVP:Doh_KDVP = Doh_KDVP(),

        @JacksonXmlProperty(localName="AttachmentHash")
        var attachmentHash:AttachmentHash? = null
    )
    {
    }
}

class Map_(
    @JsonIgnore var map:LinkedHashMap<String,String> = linkedMapOf()
)
{
    @JsonAnySetter fun set(key:String, value:String) = this.map.put(key, value)
    @JsonAnyGetter fun all(): LinkedHashMap<String,String> = this.map
    override fun hashCode() = map.hashCode()
    override fun equals(other:Any?) = when {
        this === other -> true
        other is Map_ -> map.size == other.map.size
            && map.all { (k,v) -> v.equals(other.map[k]) }
        else -> false
    }
    override fun toString() = map.toString()
}
