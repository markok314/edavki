
package si.kdvp

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.required
import org.xml.sax.SAXException
import java.io.File
import java.nio.file.Paths
import java.text.DecimalFormat
import java.time.LocalDate
import java.util.logging.Level
import java.util.logging.Logger
import javax.xml.XMLConstants
import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

enum class Currency {
    USD,
    EUR
}

data class PositionInfo(val shareId: String, val currency: Currency)

typealias PositionInfoMap = HashMap<Long, PositionInfo>

data class ClosedPosition(val id: Long,
                          val action: EAction,
                          val shareCode: String,   // eg. AMZN
                          val shareName: String,   // eg. Amazon ...
                          val amount: Double, // USD/EURs used to buy share
                          val units: Double,  // number of shares
                          val openRate: Double,
                          val closeRate: Double,
                          val profit: Double,
                          val openDate: LocalDate,
                          val closeDate: LocalDate,
                          val type: EPositionType,
                          val currency: Currency
)

typealias ClosedPositionsMap = LinkedHashMap<String, ArrayList<ClosedPosition>>

var logger: Logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME)

fun readTaxpayerInfo(xmlFileName: String): Envelope {
    val xmlMapper = XmlMapper().registerModule(KotlinModule())
        .registerModule(Jdk8Module())
        .registerModule(JavaTimeModule())

    val xml: String = File(xmlFileName).readText(Charsets.UTF_8)
    return xmlMapper.readValue(xml, Envelope::class.java)
}


// Used in the generated DohKdvpXsd.kt
class DoubleSerializer(private val numDecimals: Int = 4) : JsonSerializer<Double?>() {

    override fun serialize(value: Double?, jgen: JsonGenerator, provider: SerializerProvider) {
        val pattern = "0." + "0".repeat(numDecimals)
        val formatter = DecimalFormat(pattern)
        val output = formatter.format(value)
        jgen.writeNumber(output)
    }
}


fun saveEDavkiXml(fileName: String, envelope: Envelope) {
    val xmlMapper = XmlMapper().registerModule(KotlinModule()).
                                registerModule(Jdk8Module()).
                                registerModule(JavaTimeModule()).
                                enable(SerializationFeature.INDENT_OUTPUT).
                                // without the next line dates are written as year, month and day in
                                // 3 XML tags with the same name
                                configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false).
                                setSerializationInclusion(JsonInclude.Include.NON_NULL)

    val outFile = File(fileName)
    xmlMapper.writeValue(outFile, envelope)
    validateOutput("schemas/Doh_KDVP_9.xsd", fileName)
}


fun validateOutput(schemeFileName: String, xmlFileName: String) {
    val factory: SchemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)

    val schemaLocation = File(schemeFileName)
    val schema = factory.newSchema(schemaLocation)
    val validator = schema.newValidator()
    val source = StreamSource(xmlFileName)

    try {
        validator.validate(source)
        println("XML file '$xmlFileName' is valid.")
    } catch (ex: SAXException) {
        throw  IllegalStateException(" XML file '$xmlFileName' not valid: ${ex.message}")
    }
}


const val CONVERSIONS_FILE_PATH = "conversionRates.xml"


fun main(args: Array<String>) {

    println("This application is not finished yet! DO NOT USE IT FOR TAX REPORT!")
    println("Ta program še ni končan! NE UPORABLJAJTE GA ZA DAVČNO NAPOVED!\n")

    println("Current dir: ${Paths.get("").toAbsolutePath()}")
    val parser = ArgParser("example")
    val xmlExportedFromEDavki by parser.option(
        ArgType.String,
        shortName = "d",
        description = "datoteka izvožena iz eDavkov, ki vsebuje podatke o davčnem zavezancu in " +
                "datume. Naj bo izvoz narejen za pravilno leto ali popravi datume ročno!").required()
    val xlsxExportedFromEToro by parser.option(
        ArgType.String,
        shortName = "e",
        description = "eToro xlsx datoteka")
    val csvName by parser.option(
        ArgType.String, shortName = "c",
        description = "Csv datoteka, delno ročno pripravljena na osnovi pregleda transakcij " +
                "pri BKS. Glej denarTips.txt")
    val outXmlForEDavkiPrefix by parser.option(
        ArgType.String,
        shortName = "o",
        description = "Predpona izhodne datoteke.").required()
    parser.parse(args)

    logger.setLevel(Level.INFO);

    val taxYear = LocalDate.now().year - 1 // assume that we are creating report for prev. year
    println("Tax year: $taxYear")

    val currencyConverter = CurrencyConverter(CONVERSIONS_FILE_PATH)
    val envelopeWithTaxpayerInfo = readTaxpayerInfo(xmlExportedFromEDavki)

    val eToroClosedPositionsMap = if (xlsxExportedFromEToro != null) {
        EToroXlsxReader().readClosedPositions(xlsxExportedFromEToro as String)
    } else {
        null
    }

    val csvClosedPositionsMap = if (csvName != null) {
        CsvConverter().convert(csvName as String, taxYear)
    } else {
        null
    }

    val mergedClosedPositionsMap = if (eToroClosedPositionsMap != null  &&  csvClosedPositionsMap != null) {
        logger.info("Merging eToro and CSV ...")
        val mergedPositions = ClosedPositionsMap(eToroClosedPositionsMap)
        for (csvEntry in csvClosedPositionsMap) {
            val mergedPositionsList = mergedPositions[csvEntry.key]
            if (mergedPositionsList != null) {
                mergedPositionsList.addAll(csvEntry.value)
            } else {
                val list = ArrayList<ClosedPosition>()
                list.addAll(csvEntry.value)
                mergedPositions[csvEntry.key] = list
            }
        }
        mergedPositions
    } else eToroClosedPositionsMap ?: csvClosedPositionsMap

    if (mergedClosedPositionsMap == null) {
        throw IllegalStateException("Error: No data to save! Check input arguments.")
    }

    for (entry in mergedClosedPositionsMap) {
        println(entry.key)
        for (closedPosition in entry.value) {
            println(closedPosition)
        }
    }

    val generator = KdvpGenerator()
    val envelope = generator.createEnvelope(envelopeWithTaxpayerInfo,
        mergedClosedPositionsMap,
        currencyConverter,
        taxYear)

    val outDirName = "output"
    val outDir = File(outDirName)
    if (!outDir.exists()) {
        outDir.mkdir()
    }
    saveEDavkiXml("$outDirName/$outXmlForEDavkiPrefix-edavki.xml", envelope)
    logger.info("Done!")
}
