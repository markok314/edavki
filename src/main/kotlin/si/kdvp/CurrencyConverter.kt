package si.kdvp

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import java.io.FileInputStream
import java.lang.IllegalStateException
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse.BodyHandlers
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.attribute.BasicFileAttributes
import java.nio.file.attribute.FileTime
import java.time.Duration
import java.time.LocalDate
import java.util.*
import kotlin.collections.HashMap

typealias CurrencyConvMap = HashMap<String, Double>

/**
 * This class provides conversion rates from Bank of Slovenia.
 */
class CurrencyConverter(downloadFilePath: String) {

    private val conversionsMap: HashMap<LocalDate, CurrencyConvMap>

    init {
        downloadFromBsToFile(Path.of(downloadFilePath))
        conversionsMap = loadFromFile(downloadFilePath)
    }


    /**
     * Downloads conversion rates from Bank of Slovenia to file, but only if
     * the file is older than <current_year>-01-01, because newer conversion
     * rates are not needed for last year's tax report.
     */
    fun downloadFromBsToFile(outFilePath: Path) {
        // if file exists and is newer then the last date of previous year, do not
        // load it again, because it contains all the requested dates for conversion rates.
        if (Files.exists(outFilePath)) {
            val attr = Files.readAttributes<BasicFileAttributes>(outFilePath,
                                                                 BasicFileAttributes::class.java)
            val now = LocalDate.now()
            val firstOfThisYear = GregorianCalendar(now.year, 1, 1)
            if (attr.creationTime() >= FileTime.fromMillis(firstOfThisYear.timeInMillis)) {
                return
            }
        }

        val client: HttpClient = HttpClient.newHttpClient()

        val request = HttpRequest.newBuilder()
            .uri(URI.create("https://www.bsi.si/_data/tecajnice/dtecbs-l.xml"))
            .timeout(Duration.ofSeconds(30))
            .GET()
            .build()

        client.send(request, BodyHandlers.ofFile(outFilePath))
    }


    fun loadFromFile(xmlFileName: String): HashMap<LocalDate, CurrencyConvMap> {
        val xmlMapper = XmlMapper(JacksonXmlModule().apply { setXMLTextElementName("xmlText") })
            .registerModule(KotlinModule())
            .registerModule(Jdk8Module())
            .registerModule(JavaTimeModule())

        val convRates: DtecBsXsd = xmlMapper.readValue(FileInputStream(xmlFileName), DtecBsXsd::class.java)

        val conversionsMap = HashMap<LocalDate, CurrencyConvMap>()
        for (convRatesForDay in convRates.tecajnica) {
            val convRatesForCurrencyMap = CurrencyConvMap()
            for (convRatesForCurrency in convRatesForDay.tecaj) {
                convRatesForCurrencyMap[convRatesForCurrency.oznaka] = convRatesForCurrency.conversionRate
            }
            conversionsMap[convRatesForDay.datum] = convRatesForCurrencyMap
        }
        return conversionsMap
    }


    fun getConversionRate(date: LocalDate, currency: Currency): Double {

        if (currency == Currency.EUR) {
            return 1.0
        }

        // for some dates, which are holidays in Slovenia but not in US,
        // there is no conversion rate. However, on web interface it
        // shows conversion rate from the previous date for dates without
        // conversion rate.
        var convMapForDate = conversionsMap[date]
        var safetyCounter = 10
        var convDate = date

        while (convMapForDate == null) {
            convDate = convDate.minusDays(1)
            convMapForDate = conversionsMap[convDate]

            safetyCounter--
            // detect illegal state instead of freezing in an endless loop
            if (safetyCounter == 0) {
                throw IllegalStateException("There is no conversion rate fo date $date and ten previous days!")
            }
        }

        val convRate: Double = convMapForDate[currency.name]
            ?: throw IllegalStateException("There is no conversion rate for ${currency.name} " +
                                                   "for date $date!")

        return convRate
    }
}