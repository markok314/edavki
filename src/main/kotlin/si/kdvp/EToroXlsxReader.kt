package si.kdvp

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import java.io.FileInputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.logging.Level
import java.util.logging.Logger


enum class EAction {  // TODO rename to TransactionType
    Buy, Sell
}

enum class EPositionType {
    Stocks, CFD
}

enum class EClosedPosCols(val colIdx: Int) {
    `Position ID`(0),
    Action(1),
    Amount(2),
    Units(3),
    `Open Rate`(9),
    `Close Rate`(10),
    // Spread(7)  // not used
    Profit(8),
    `Open Date`(4),
    `Close Date`(5),
    // `Take Profit Rate`(11),
    `Stop lose rate`(12),
    `Rollover Fees and Dividends`(13),
    `Type`(15),
    `ISIN`(16)
}

enum class TransactReportCols(val colIdx: Int) {
    Type(1),
    Details(2),
    `Position ID`(7)
}


class EToroXlsxReader {

    private var logger: Logger = Logger.getLogger(EToroXlsxReader::class.java.name)

    init {
        logger.level = Level.INFO
    }

    private fun cellToDouble(row: Row, column: EClosedPosCols): Double {
        // return row.getCell(column.colIdx).stringCellValue.toDouble()  all numbers used to be strings in 2020
        return row.getCell(column.colIdx).numericCellValue
    }

    private fun strCellToDouble(row: Row, column: EClosedPosCols): Double {
        return row.getCell(column.colIdx).stringCellValue.toDouble()
    }


    private fun cellToLong(row: Row, column: EClosedPosCols): Long {
        return row.getCell(column.colIdx).stringCellValue.toLong()
    }


    private fun cellToDate(row: Row, column: EClosedPosCols): LocalDate {
        val dateTimeStr = row.getCell(column.colIdx).stringCellValue
        val format = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")
        return LocalDate.parse(dateTimeStr, format)
    }


    /**
     * Checks that we will use the right indices when getting information from
     * table.
     */
    private fun checkColumnNames(row: Row?) {
        row ?: throw IllegalArgumentException("The first row in table 'Closed Positions' is null!")
        for (enumItem in EClosedPosCols.values()) {
            val colName = row.getCell(enumItem.colIdx).stringCellValue
            if (colName != enumItem.name) {
                throw IllegalArgumentException("Column name '$colName' does not match enum item " +
                                                       "'${enumItem.name}' with index " +
                                                       "${enumItem.colIdx} in table 'Closed Positions'!")
            }
        }
    }


//    fun readXlsx(fileName: String): ClosedPositionsMap {
//        val fis = FileInputStream(fileName)
//        val workbook: Workbook = WorkbookFactory.create(fis)
//    }

    fun readClosedPositions(fileName: String): ClosedPositionsMap {
        val fis = FileInputStream(fileName)
        val workbook: Workbook = WorkbookFactory.create(fis)

        val positionInfoMap = readTransactionsReport(workbook)

        val sheet: Sheet = workbook.getSheet("Closed Positions")
        val closedPositions = LinkedHashMap<String, ArrayList<ClosedPosition>>()

        for ((rowIdx, row) in sheet.withIndex()) {
            if (rowIdx == 0) {
                checkColumnNames(row)
            } else {
                val positionId = row.getCell(EClosedPosCols.`Position ID`.colIdx).stringCellValue.toLong()
                val positionInfo = positionInfoMap[positionId] ?: throw IllegalStateException("No position " +
                        "info for position ID: $positionId")
                val cell = row.getCell(EClosedPosCols.Action.colIdx)
                val actionAndShareName = cell.stringCellValue.split(' ', limit = 2)
                val action = when (actionAndShareName[0]) {
                    "Buy" -> EAction.Buy
                    "Sell" -> EAction.Sell
                    else -> throw IllegalArgumentException("Unknown action: ${actionAndShareName[0]}")
                }

                val positionType = when (row.getCell(EClosedPosCols.`Type`.colIdx).stringCellValue) {
                    "Stocks" -> EPositionType.Stocks
                    "CFD" -> EPositionType.CFD
                    else ->
                        throw IllegalArgumentException("Unknown position type: " +
                                                       row.getCell(14).stringCellValue)
                }

                // openRate and closeRate are given also in eToro data, but they sometimes slightly differ
                // from values calculated this way (also masbug does this). I decided to use calculated
                // values, because amount is typically integer, while units are given with 6 decimals.
                val amount = cellToDouble(row, EClosedPosCols.Amount)
                val units = strCellToDouble(row, EClosedPosCols.Units)
                val profit = cellToDouble(row, EClosedPosCols.Profit)
                val openRateUSD = amount / units
                val closeRateUSD = (amount + profit) / units

                val position =
                    ClosedPosition(cellToLong(row, EClosedPosCols.`Position ID`),
                                   action,
                                   shareCode = positionInfo.shareId,
                                   shareName = actionAndShareName[1],
                                   amount,
                                   units,
                                   openRateUSD,
                                   closeRateUSD,
                                   profit,
                                   openDate = cellToDate(row, EClosedPosCols.`Open Date`),
                                   closeDate = cellToDate(row, EClosedPosCols.`Close Date`),
                                   type = positionType,
                                   currency = Currency.USD // positionInfo.currency  // amount and profit are always in USD
                                  )

                val shareId = position.shareCode
                val positionsList = closedPositions[shareId]
                if (positionsList == null) {
                    val list = ArrayList<ClosedPosition>()
                    list.add(position)
                    closedPositions[shareId] = list
                } else {
                    positionsList.add(position)
                }
                logger.finest("eToroPosition: $position")
            }
        }

        fis.close()

        return closedPositions
    }

    // private val TRANSACT_REPORTS_TABLE = "Transactions Report"  // name in 2020
    private val TRANSACT_REPORTS_TABLE = "Account Activity"

    private fun readTransactionsReport(workbook: Workbook): PositionInfoMap {

        val sheet: Sheet = workbook.getSheet(TRANSACT_REPORTS_TABLE)
        val positionInfoMap = PositionInfoMap()

        for ((rowIdx, row) in sheet.withIndex()) {
            if (rowIdx == 0) {
                checkTransReportColumnNames(row)
            } else {
                val cell = row.getCell(TransactReportCols.Type.colIdx)

                val transactType = cell.stringCellValue.trim()
                if (transactType != "Open Position"  &&  transactType != "Profit/Loss of Trade") {
                    continue
                }

                val details = row.getCell(TransactReportCols.Details.colIdx).stringCellValue
                val detailsList = details.split('/')
                if (detailsList.size != 2) {
                    throw IllegalStateException("Invalid 'Details' column in " +
                            "sheet $TRANSACT_REPORTS_TABLE, row $rowIdx. " +
                            "Expected: <shareID/currency>, eg. TSLA/USD, got: $details")
                }
                val positionId = row.getCell(TransactReportCols.`Position ID`.colIdx).stringCellValue.toLong()
                positionInfoMap[positionId] = PositionInfo(detailsList[0].uppercase(), // share codes are typically
                                                                      // upper case, but not VOW.de in eToro output
                                                           Currency.valueOf(detailsList[1]))
            }
        }
        return positionInfoMap
    }


    private fun checkTransReportColumnNames(row: Row?) {
        row ?: throw IllegalArgumentException("The first row in table '$TRANSACT_REPORTS_TABLE' is null!")
        for (enumItem in TransactReportCols.values()) {
            val colName = row.getCell(enumItem.colIdx).stringCellValue
            if (colName != enumItem.name) {
                throw IllegalArgumentException("Column name '$colName' does not match enum item " +
                        "'${enumItem.name}' with index " +
                        "${enumItem.colIdx} in table $TRANSACT_REPORTS_TABLE!")
            }
        }
    }

}