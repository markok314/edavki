Vsebina na disku
===

1. `edavki/etoro-bks-edavki` (Kotlin) ==> KDVP
   Moj program, ki zna združiti poročilo BKS in eToro v en KDVP. Ne zna
   še obdelati D-IFI in Doh-Div, za kar uporabljam etoro-masbug.
   Pretvori eToro xlsx zapis in BKS csv zapis v KDVP dokument, ki je
   primeren za oddajo v eDavke.

2. `edavki/etoro-masbug` - uporabljam za testiranje svojega programa
    (KDiff3 na obeh KDVP datotekah) in izdelavo D-IFI in Doh-Div.

3. `edavki/etoro-markok314` (na GitHubu, lokalna kopija pobrisana) - moj
    fork etoro-masbug, kjer je popravljen Doh-Div
    tako da izračuna bruto dividende iz eToro podanih neto.
    Pull request za fix je bil sprejet (da so dividende podane v bruto znesku),
    zato ta verzija ni več aktualna.


Vrste kapitalskih dohodkov za prijavo
===

1. Stanovanje - najemnina, Doh-Prem
2. Dividende oz. prodaja iSystem
3. Generali Skladi v primeru prodaje - KDVP
4. BKS delnice - KDVP
5. eToro delnice = KDVP, Doh-Div, Doh-IFI

Postopek
===

1. __Pripravi vhodne datoteke__. 
   - za stanovanje sam pripraviš podatke
   - za iSystem dobiš dividende po mailu, shrani v doc/KDSkladi/202x
   - Generali predpripravi vpisni list in ga imaš že v eDavkih
   - BKS: 
     - login
     - tab _PORTFELJ_
     - tab _Pregled transakcij_
     - vpiši _Obdobje_, npr:: `01.01.<preteklo leto>  31.12.<preteklo leto>`
       __Pomembno__: Izberi obdobje dovolj daleč nazaj, da bodo prisotni
       nakupi vseh delnic prodanih v preteklem letu. Npr. če je bil VOW
       prodan 2021 kupljen v letu 2020, mora tabela zajemati tudi leto 2020.
       Stare prodaje briši ročno, da se ne bodo pojavile v napovedi.
     - Gumb _Izvedi_
     - copy-paste tabelo _Vrednostni papirji_ od vključno 
       headerja do zadnje vrstice v datoteko 
       `<leto>/inputFiles/<leto>-bks-vrednostni-papirji.csv`.
     - Find/replace `space tab` s '; ' (podpičje presledek)
     - shrani
     - Nato isti copy-paste-replace postopek za tabelo _Denar_, le da jo
       shraniš v datoteko s končnico `csv-denar` (hardcoded). Od tu se
       pobere podatek o valuti (USD/EUR).
   - eToro 
     - Login
     - Iz menija na levi izberi `Settings`
     - Izberi `Account`
     - Pod `Account Statement` klikni gumb `View`.
     - Izberi datum `1.1.<preteklo leto>` do `1.1.<tekoče leto>`.
       Ker podatke podaja drugače kot BKS, ni potreben 
       izvoz za nazaj.
     - Izvozi v Excel formatu (ikona desno zgoraj) v datoteko 
       `eToro-account-statement-<start date>-<end date>.xlsx`

2. Pretvori vhodne datoteke v xml datoteke za eDavke
   - V IntelliJ naredi novo run konfiguracijo s pravimi imeni vhodnih datotek.
     Poženi ta program iz IntelliJ. Odpravi najdene napake zaradi spremembe
     formata datotek. eToro je npr. iz 2020 do 2021 premaknil in preimenoval 
     nekaj stolpcev, format je iz stringov ze večino stolpcev spremenil v 
     double.
     Primer ukazne vrstice:
     `-d ~/doc/KDSkladi/2020/inputFiles/Doh_KDVP-izvoz-osebni-podatki.xml -e ~/doc/KDSkladi/2020/inputFiles/2020-eToroAccountStatement-pilot100kg-01-01-2020-31-12-2020.xlsx -c ~/doc/KDSkladi/2020/inputFiles/bks-2020.csv -o kdvp`
   - Validacija dokumenta glede na XSD ne uspe, ker ima element body 
     atribut xmlns="".
     Zato postavi breakpoint za shranjevanjem in pred validacijo, poženi program
     v debug načinu, ko se ustavi na BP, odpri generirani xml iz `edavki/output`
     v urejevalniku in pobriši omenjeni atribut. Nadaljuj z izvajanjem programa.
     Alternativno poženi program prvič, nato zakomentiraj shranjevanje v Main.kt:87
     in poženi program še enkrat.
   - Ročno preveri vsebino headerja in zapise za delnico ali dve.
   - Poženi masbug program (leta 2023 pri oddajanju za 2022 je bila tezava v
     verziji Pythona in knjiznic. Malo sem pohekal knjiznice (from collections import ...)
     pa je šlo. Prihodnje leto posodobi venv na zadnje verzije knjižnic iz requirements.txt
     in ustrezno popravi program).

         cd ~/proj/edavki/edavki-masbug
         python -m venv venv
         source venv/bin/activate
         pip3 install --upgrade git+https://github.com/masbug/etoro-edavki.git
         mkdir 2021; cd 2021
         etoro-edavki ~/doc/KDSkladi/2021/inputFiles/etoro-account-statement-1-1-2021-12-31-2021.xlsx

   - Primerjaj KDVP izhod mojeaga programa s pomočjo KDiff3.
     Znane razlike:
     - F8 (zaloga VP) se v mojem programu ne računa, ker naj bi jo izračunal FURS oz.
       za davek ni pomembna. Lani zaradi tega ni bilo težav.
     - Napaka v tem programu: - število delnic v
       `<SecurityCount>19</SecurityCount>`
       je lahko preveliko, če so v vhodni datoteki tudi CDFji, ker CDFje 
       ignorira šele ko pripravlja KDVP, glej KdvpGenerator.kt:  
       `if (kdvpItem.securities?.rows?.isNotEmpty() == true) {   // ETFs have this empty`
       Popravi tako, da CDFjev sploh ne prebereš iz tabele. Ni pa ta napaka 
       kritična - uvoz v eDavke uspe.
   - Ročno preveri D_IFI
   - Ročno preveri Doh-Div in dodaj manjkajoče podatke, kot so naslovi 
     podjetij in mednarodne pogodbe. Glej `Company_info.xlsx` iz preteklega
     leta za svoja podjetja. Ni pa težko dopolniti programa, ki bo dodal
     tudi mednarodne pogodbe. Glej še poglavje __Dividende__ spodaj.

3. Uvozi izhodne datoteke v eDavke
   Postopek je dokaj enostaven, čeprav imajo eDavki kar nekaj trikov.

   - Doh-KDVP in D-IFI uvoziš tako, da odpreš nov dokument Doh-KDVP oz. D-IFI,
     in nato izbereš 'Uvoz popisnega lista'.

   - V DohKDVP in D-IFI lahko dodaš več popisnih listov. Lahko torej uvoziš
     več XMLjev, vendar v isti dokument.

   - Za Doh-Div na eDavkih ne izbereš obrazca, ampak takoj po vstopu na eDavke klikneš
     na povezavo `Dokumenti` in nato `Uvoz`. Edavki bodo sami prepoznali vrsto obrazca
     in uvozili vsebino XML datoteke.

   - Ker ne smeš oddati dveh dokumentov istega tipa, npr. Doh-Div, to
     pomeni, da moraš isys delnice ročno dopisati v Doh-Div uvožen iz 
     eTora. 

   - isys pdfje in eToro xlsx dodaj kot priloge v Doh-Div kot dokazilo o 
     plačilu davka v tujini. Št. pogodbe in člena moraš vpisati ročno,
     za ZDA: '10/01, 10. člen'. Glej povezavo
     https://www.gov.si/drzavni-organi/ministrstva/ministrstvo-za-finance/o-ministrstvu/direktorat-za-sistem-davcnih-carinskih-in-drugih-javnih-prihodkov/seznam-veljavnih-konvencij-o-izogibanju-dvojnega-obdavcevanja-dohodka-in-premozenja/
     za ostale države.

   - Pozor! Pri Doh-KDVP se vam bo verjetno prikazalo opozorilo (rumen trikotnik)
     pri popisnih listih. Razlog je v tem, da imate pri vseh transakcijah privzeto
     kljukico v stolpcu 10 (Pravilo iz drugega odstavka v povezavi s petim
     odstavkom 97. člena ZDoh-2). Ta govori o nezmanjšanju pozitivne davčne osnove.

4. Stanovanje
   - Odpreš obrazec Doh-Prem, in obdržiš ponujene lanske podatke.
   - Klikneš gumb `Spremeni` in te lepo vodi skozi vpis podatkov.


Dividende - posebnosti
===

Povzetek iz http://slotrade.blogspot.com/2018/02/prijava-tujih-dividend-v-edavke.html
---

Po prijavi v eDavke izberemo papir DOH-DIV.

Podatki, ki jih potrebujemo, so naslednji:

_Datum dividende_ - datum, ko smo prejeli dividendo.

_Dividenda_ - znesek dividende v evrih. Če smo dividendo prejeli v drugi
valuti, moramo znesek preračunati v evre po ECB tečajnici na tisti
dan. Tečaj izplačevalca ali eTora torej ne velja.

_Tuji davek_ - če smo že plačali davek v tujini, moramo navesti znesek
plačanega davka, prav tako v evrih. Ta davek nam je po vsej
verjetnosti trgal že sam broker (zagotovo velja za ZDA), torej moramo
v izpisku, ki ga dobimo od brokerja, samo pogledati, koliko so nam
trgali.

_Država_ - država, v kateri smo dobili dividendo in plačali davek.

_Uveljavljam oprostitev po mednarodni pogodbi_ - če smo plačali davek v
tujini in ima Slovenija s to državo sporazum o izogibanju dvojnega
obdavčevanja (in zelo verjetno ga ima), potem moramo navesti številko
pogodbe, ki jo najdemo na tem spisku. Na primer, za ZDA je to pogodba
z oznako 10/01, ki je na tem naslovu in 10. člen o dividendah pravi,
da v tuji državi plačamo 15% dividende, doma pa potem samo še
10%. Sicer nam to ob branju tega člena ni niti približno jasno, ampak
tako pač je;)

_Podatki o izplačevalcu dividend (firmi)_ - tu v osnovi rabimo celotni
naziv podjetja, naslov in davčno številko oziroma identifikacijsko
številko, kar v primeru US firm pomeni IRS employer identification
number. Kje vse to dobimo? Vsi podatki so na voljo v raznih
dokumentih, ki jih objavljajo vsa podjetja. Praktično vsaka firma ima
na svoji spletni strani stran z imenom "Investor relations" ali kaj
podobnega, kjer je ponavadi spisek raznih annual reportov ali SEC
dokumentov. Še najlažje podatke dobimo iz annual reporta

_Potrdila_ - na koncu moramo dodati še dokazila o vsem, kar smo
vnesli. To je nekaj, kar moramo dobiti od brokerja (statements,
transactions, itd.). V mojem primeru sem dodal mesečna poročila
(Statements), v katerih so navedene vse dividende.

Zanimiva vporašanja:

1) Dobil sem obvestilo mojega brokerja, da je Vanguard za moj ETF dne
   18.12 delil dividende, no jaz sem to dividendo (in tudi obvestilo o
   tem) na svoj račun prejel šele 11.1.2021. Torej prijavim dividendo
   letos ali šele drugo leto ?

   Odgovor:
   Drugo leto

2. Vprašanje glede dividend pa je: eToro odbije od "izplačila"
   (nakazila na available ballance) dividende klasično ameriško 30%
   obdavčitev.
   https://www.etoro.com/customer-service/help/75781345/does-etoro-pay-dividends/

   V izogib dvojnemu obdavčevanju torej jaz ne plačam nič, moram pa to
   prijaviti?

   Odg:
   Torej, tu je odgovor tako iz eTora kot tudi FURSa.
   Prvih ne zanima kaj dosti, kaj imajo zmenjene države med seboj.
   So brezplačna platforma in to je to, kar dobimo za ta denar. Torej,
   davek je 30%. FURSa pa ne zanima in ni dolžan preverjati, ali je
   bil davek v tujini pravilno obračunan. In zategnejo še svojih 12,5%.

Ročne dopolnitve za Doh-Div 
---

etoro-masbug izpiše:

------------------------------------------------------------------------------------------------------------------------------------

Za uveljavljanje olajšave pri eDavkih, je potrebno izračunat koliko davka 
je bilo odvedenega,
pripisat konvencijo (MP, št., člen, odstavek) o preprečevanju dvojnega 
obdavčevanja in priložiti dokazila o plačanem davku...

Za davek po državah in konvencije glej:
https://www.gov.si/drzavni-organi/ministrstva/ministrstvo-za-finance/o-ministrstvu/direktorat-za-sistem-davcnih-carinskih-in-drugih-javnih-prihodkov/seznam-veljavnih-konvencij-o-izogibanju-dvojnega-obdavcevanja-dohodka-in-premozenja/.

Dodatni info: 
https://www.etoro.com/customer-service/help/1484910272/how-much-tax-is-deducted-from-my-dividends/

------------------------------------------------------------------------------------------------------------------------------------
Manjkajo podatki o podjetjih za sledeče delnice:

ABT; US0028241000; Abbott Laboratories; Abbott Park, Illinois, USA
AWK; US0304201033; American Water Works Co Inc; Camden, New Jersey, USA
QQQ; US46090E1038; Invesco QQQ; Two Peachtree Pointe, 1555 Peachtree Street, N.E., Suite 1800, Atlanta, Georgia 30309, USA

Dodaj (in dopolni) zgornje podatke v Company_info.xlsx in ponovno poženi konverzijo! (Lahko nadaljuješ z oddajo, vendar bo potrebno te podatke ročno vnesti na eDavki.)
Podatke o naslovu je običajno mogoče poiskati z ISIN kodo ali simbolom na https://www.marketscreener.com/ pod zavihkom "Company".
------------------------------------------------------------------------------------------------------------------------------------

Povezave
===
etoro-masbug: https://github.com/masbug/etoro-edavki

Spisek držav s sklenjenimi sporazumi v izogib dvojnemu obdavčevanju:
https://www.fu.gov.si/fileadmin/Internet/Davki_in_druge_dajatve/Podrocja/Mednarodno_obdavcenje/Zakonodaja/Seznam_veljavnih_MP.doc
https://www.google.com/url?q=https://www.fu.gov.si/fileadmin/Internet/Davki_in_druge_dajatve/Podrocja/Mednarodno_obdavcenje/Zakonodaja/Seznam_veljavnih_MP.doc&sa=D&source=docs&ust=1645738327726583&usg=AOvVaw3psizqLdLxxIDiUGunRvUk

Države in pogodbe o izogibanju dvojnega obdavčevanja:
https://www.gov.si/drzavni-organi/ministrstva/ministrstvo-za-finance/o-ministrstvu/direktorat-za-sistem-davcnih-carinskih-in-drugih-javnih-prihodkov/seznam-veljavnih-konvencij-o-izogibanju-dvojnega-obdavcevanja-dohodka-in-premozenja/

eDavki XML sheme (XSD):
http://edavki.durs.si/Documents/Schemas/EDP-Common-1.xsd
http://edavki.durs.si/Documents/Schemas/Doh_KDVP_9.xsd

JU's eToro Converter
https://docs.google.com/document/d/10Sng94EPvkSOgpeEQ_6wsd33RbfFpnO_bUokLqT5GWQ/edit

Programerski namigi
===

Kako ne moreš pognati programa iz ukazne vrstice:
---

1. V build/scripts sta POSIX shell skripta in Windows bat. V obeh
   je pot do davki.jar napačna, saj vsebuje dir `lib`, medtem ko se davki.jar
   nahaja v `libs`. Ostalih jar datotek pa sploh ni v build direktoriju, ampak
   so v ~/.gradle cacheu.

2. Način brez skripte:

       cd ~/proj/edavki
       gradle clean
       gradle build
       cd build/libs
       // jar knjižnice so precej od rok
       kotlin -cp davki.jar:/home/markok/.gradle/caches/modules-2/files-2.1/org.jetbrains.kotlinx/kotlinx-cli-jvm/0.3.4/a4ad829fe8b3662f81df5f4d86598e48f41b8477/kotlinx-cli-jvm-0.3.4.jar si.kdvp.MainKt

Tole še vedno ne deluje, ker ne najde java.net.http.HttpClient. Razlog je
verjetno v tem, da kotlin ne podpira modulov, ta razred pa je bil dodan v
JRE 11.

__Trenutno je edini način za zagon iz InelliJIDEA.__ AndroidStudio ni uporaben!


Info about schema-gen and Jackson
---

Now when classes are generated and debugged, minor changes should be done directly
in the generated classes, not all classes generated again.

To generate Kotlin classes out of schemas:
1. Run schema-gen:

       cd schema-gen
       # edit KotlinGenMain and set filenames there.
       gradle build
       # modify EDP-Common-1.xsd to avoid error 'bodyContent has no type':
       #   <xs:element name="bodyContent" />
       # to:
       #   <xs:element name="bodyContent" type="xs:string" />
       cp schema-gen/build/libs/schema-gen.jar ~/bin/groovy-3.0.7/lib
       cd ~/bin/groovy-3.0.7/lib
       java -cp schema-gen.jar:groovy-3.0.7.jar:groovy-xml-3.0.7.jar com.javagen.schema.kotlin.KotlinGenMain

2. KAXB from github seems to be unfinished project which produces nothing.

Fixes required because of bugs in schema-gen:
3. TypeInventoryEnum.Unknown is used but not generated. Add it to the enum.
4. Most fields are missing @JacksonXmlProperty(localName="" because of case difference
   between XML schema and Kotlin source code.
5. XSD element choice is not translated properly - only one element 'value' is generated.
6. Write annotations from XSD as comments in the generated code.
7. If variable has name with inverse case, for example kDVP, is is serialized to XML tag
   'kdvp' regardless of localname='KDVP'. Solution: rename var to lower case ('kdvp').
   Update: It seems that XML name is lowercase if the first letter of var name
   is lower case, and the second is upper case. The rest is not important.
8. Add @JsonSerialize(using = DoubleSerializer::class) (see class DoubleSerializer below)
   when schema specifies the number of digits.
9. Add @JsonPropertyOrder annotations according to order defined in schema.
10. When converting schema for conversion rates from BS, maxoccurs="unbounded" caused no
    code to be generated. There was WARNING: IGNORING WRAPPER CLASS: DtecBS

Links for jackson:

    https://github.com/FasterXML
    https://stackify.com/java-xml-jackson/
    https://www.baeldung.com/kotlin/jackson-kotlin
    https://www.baeldung.com/jackson-ignore-null-fields
    https://github.com/FasterXML/jackson-module-kotlin/issues/138 - solution for InvalidDefinitionException: Invalid definition for property ''
    https://www.programmersought.com/article/20664460073/

User experience with jackson: I was able to get the desired form of XML with indentation,
namespaces (although without prefix), proper serialization of LocalDate and order
of elements. However, it required big amount of googling. There are many tutorials on
the internet, but most if not all of them cover only basic functionality. And most of them
are for Java, not Kotlin, but there is not much difference (I found it only in String[]
value in annotation @JsonPropertyOrder(value = ["ID","F8"]) or @JsonPropertyOrder("ID","F8"),
which is specified as @JsonPropertyOrder({"ID","F8"}) in Java. I'm also not sure what's the
difference between nullable and non-nullable properties from XML point of view.
